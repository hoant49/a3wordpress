<?php


use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\DemoSites\DemoSitesRepository;
use Kubio\PluginsManager;

?>

	<div class="kubio-admin-page-page-section">
		<div class="<?php kubio_admin_page_class( 'templates-wrapper' ); ?>">
			<div id="kubio-templates-list" class="<?php kubio_admin_page_component_class( 'templates' ); ?>">
				<?php foreach ( DemoSitesRepository::getDemos() as $demo ) : ?>
					<div class="<?php kubio_admin_page_component_class( 'template' ); ?>"
						 data-demo-site="<?php echo esc_attr( Arr::get( $demo, 'slug', '' ) ); ?>">

						<div class="<?php kubio_admin_page_component_class( 'template-image' ); ?>">
							<img src="<?php echo esc_url( Arr::get( $demo, 'thumb', kubio_url( '/static/admin-pages/' ) ) ); ?>"
								 alt="image">
						</div>

						<div class="<?php kubio_admin_page_component_class( 'template-body' ); ?>">
							<div class="<?php kubio_admin_page_component_class( 'template-title' ); ?>">
								<?php echo esc_html( Arr::get( $demo, 'name', __( 'Untitled', 'kubio' ) ) ); ?>
							</div>

							<div class="<?php kubio_admin_page_component_class( 'template-buttons' ); ?>">
								<button class="button button-primary"
										data-slug="<?php echo esc_attr( Arr::get( $demo, 'slug', '' ) ); ?>">
									<?php esc_html_e( 'Import site', 'kubio' ); ?>
								</button>

								<?php if ( Arr::get( $demo, 'preview', 'null' ) ) : ?>
									<a href="<?php echo esc_url( Arr::get( $demo, 'preview' ) ); ?>" target="_blank"
									   rel="nofollow"
									   class="button ">
										<?php esc_html_e( 'Preview', 'preview' ); ?>
									</a>
								<?php endif; ?>
							</div>

						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<div id="kubio-template-installing"
				 class="<?php kubio_admin_page_component_class( 'template-installing', array( 'hidden' ) ); ?>">
				<div class="<?php kubio_admin_page_component_class( 'template-installing-wrapper' ); ?>">
					<div class="kubio-admin-row">
						<div class="<?php kubio_admin_page_component_class( 'template-installing-image-col' ); ?>">
							<div class="<?php kubio_admin_page_component_class( 'template-image' ); ?>">
								<img/>
							</div>
						</div>
						<div class="<?php kubio_admin_page_component_class( 'template-installing-info-col' ); ?>">
							<h1 data-title></h1>

							<div data-info
								 class="<?php kubio_admin_page_component_class( 'template-installing-info' ); ?>">
								<h2><?php esc_html_e( 'You are about to import a demo site', 'kubio' ); ?></h2>
								<ul>
									<li>
										<?php esc_html_e( 'Current pages will be moved to trash. You can restore the content back at any time.', 'kubio' ); ?>
									</li>
									<li>
										<?php esc_html_e( 'Posts, pages, images, widgets, menus and other theme settings will get imported.', 'kubio' ); ?>
									</li>
									<li class="text-danger">
										<?php esc_html_e( 'Your current design will be completely overwritten by the new template. This process is irreversible. If you wish to be able to go back to the current design, please create a backup of your site before proceeding with the import.', 'kubio' ); ?>
									</li>
								</ul>

								<div class="hidden" data-plugins>
									<hr/>
									<h2><?php esc_html_e( 'The following plugins will be installed as they are part of the demo', 'kubio' ); ?></h2>
									<ul data-plugins-list
										class="<?php kubio_admin_page_component_class( 'template-plugins-list' ); ?>">
									</ul>
								</div>
							</div>
							<div data-progress
								 class="<?php kubio_admin_page_component_class( 'template-installing-progress', array( 'hidden' ) ); ?>">

								<ul data-progress-list>
									<li data-installing-plugins>
										<?php esc_html_e( 'Installing required plugins', 'kubio' ); ?>
									</li>
									<li data-preparing-for-import>
										<?php esc_html_e( 'Preparing for demo site import', 'kubio' ); ?>
									</li>
									<li data-importing-content>
										<?php esc_html_e( 'Importing content', 'kubio' ); ?>
									</li>
								</ul>

								<div data-importing-errors
									 class="<?php kubio_admin_page_component_class( 'template-installing-errors', array( 'hidden' ) ); ?>">
									<p><?php esc_html_e( 'Errors', 'kubio' ); ?></p>
									<div data-importing-errors-content>

									</div>
								</div>
							</div>

							<div data-install-buttons
								 class="<?php kubio_admin_page_component_class( 'template-installing-buttons' ); ?>">
								<button class="button " data-cancel-import>
									<?php esc_html_e( 'Cancel', 'kubio' ); ?>
								</button>
								<button class="button button-primary" data-start-import>
									<?php esc_html_e( 'Import site', 'kubio' ); ?>
								</button>

							</div>

							<div data-install-success-buttons
								 class="<?php kubio_admin_page_component_class( 'template-installing-success-buttons', array( 'hidden' ) ); ?>">
								<div class="kubio-admin-row">
									<div class="kubio-admin-col">
										<p><?php esc_html_e( 'Demo site imported sucessfuly', 'kubio' ); ?></p>
									</div>
									<div class="kubio-admin-col">

										<a target="_blank" href="<?php echo esc_url( site_url() ); ?>"
										   class="button " t>
											<?php esc_html_e( 'View site', 'kubio' ); ?>
										</a>
										<a href="<?php echo esc_url( add_query_arg( 'page', 'kubio', admin_url( 'admin.php' ) ) ); ?>"
										   class="button button-primary">
											<?php esc_html_e( 'Start editing', 'kubio' ); ?>
										</a>

									</div>
								</div>


							</div>

						</div>
					</div>
					<?php kubio_print_continous_loading_bar( true ); ?>
				</div>
			</div>
		</div>
	</div>

<?php

function kubio_admin_page_templates_js_init() {
	$demos = DemoSitesRepository::getDemos();

	$plugins_states = array();

	foreach ( $demos as $demo ) {
		$plugins = Arr::get( $demo, 'plugins', array() );
		foreach ( $plugins as $plugin ) {
			$slug = Arr::get( $plugin, 'slug', null );
			if ( $slug && ! isset( $plugins_states[ $slug ] ) ) {
				$plugins_states[ $slug ] = PluginsManager::getInstance()->getPluginStatus( $slug );
			}
		}
	}

	$data = array(
		'ajax_url'       => admin_url( 'admin-ajax.php' ),
		'demos'          => $demos,
		'ajax_nonce'     => wp_create_nonce( 'kubio-ajax-demo-site-verification' ),
		'texts'          => array(
			'importing_template' => '%s',
			'plugins_states'     => array(
				'ACTIVE'        => esc_html__( 'Active', 'kubio' ),
				'INSTALLED'     => esc_html__( 'Installed', 'kubio' ),
				'NOT_INSTALLED' => esc_html__( 'Not Installed', 'kubio' ),
			),
			'import_stopped'     => esc_html__( 'Import stopped', 'kubio' ),
		),
		'plugins_states' => $plugins_states,
	);

	wp_add_inline_script(
		'kubio-admin-area',
		sprintf( 'kubio.adminArea.initDemoImport(%s)', wp_json_encode( $data ) ),
		'after'
	);
}

add_action( 'admin_footer', 'kubio_admin_page_templates_js_init', 0 );

