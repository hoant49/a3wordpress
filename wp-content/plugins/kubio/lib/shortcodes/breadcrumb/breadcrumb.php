<?php
add_shortcode( 'kubio_breadcrumb_element', 'kubio_breadcrumb_element_shortcode' );
function kubio_breadcrumb_element_shortcode( $atts ) {
	$kubio_breadcrumb_index = intval( get_theme_mod( 'kubio_breadcrumb_element_index', 0 ) );
	//set_theme_mod( 'kubio_breadcrumb_element_index',
	//	$kubio_breadcrumb_index === PHP_INT_MAX ? 0 : $kubio_breadcrumb_index + 1 );
	$atts                 = shortcode_atts(
		array(
			'id'               => 'kubio-breadcrumb-' . ( $kubio_breadcrumb_index ),
			'separator_symbol' => '/',
			'prefix'           => '',
			'home_as_icon'     => '0',
			'home_icon'        => '',
			'home_label'       => '',
			'use_prefix'       => '1',
		),
		$atts
	);
	$breadcrumb_separator = urldecode( $atts['separator_symbol'] );
	$use_prefix           = $atts['use_prefix'];
	if ( $use_prefix ) {
		$breadcrumb_prefix = kubio_breadcrumb_convert_str_space_to_html( $atts['prefix'] );
	}
	$home_as_icon            = ! ! $atts['home_as_icon'];
	$home_icon               = $atts['home_icon'];
	$lana_breadcrumb_options = array(
		'home_as_icon' => $home_as_icon,
		'home_label'   => $atts['home_label'],
	);

	if ( $home_icon && is_string( $home_icon ) ) {
		$icon_folder_name = explode( '/', $home_icon );
		$svg_file         = ( KUBIO_ROOT_DIR . 'static/icons/' . sanitize_file_name( $icon_folder_name[0] ) . '/' . sanitize_file_name( $icon_folder_name[1] ) . '.svg' );
		if ( file_exists( $svg_file ) ) {
			$svg = file_get_contents( $svg_file );
		}
	}
	if ( $home_icon ) {
		$lana_breadcrumb_options['home_icon'] = $svg;
	}
	ob_start();

	?>

	<div class="<?php echo esc_attr( $atts['id'] ); ?>-dls-wrapper breadcrumb-items__wrapper">
		<?php if ( $use_prefix ) : ?>
			<span class="breadcrumb-items__prefix"><?php echo esc_html( $breadcrumb_prefix ); ?></span>
		<?php endif; ?>
		<?php
		/**
		 *  Workaround to issue with wp_query flags. It does not put the is_page or is_home flags because the parse_query that
		 * sets this flags doesn't set the is_page or is_home flag if the is_single one is set. We are creating the query
		 * for shortcodes in this function: "shortcodeRefresh"
		 */
		global $post;
		global $wp_query;
		$modifiedFlags = array( 'is_page', 'is_home', 'is_single', 'is_singular' );
		$backup_flags  = array();

		//backup wp_query flags
		foreach ( $modifiedFlags as $flag ) {
			$backup_flags[ $flag ] = $wp_query->{$flag};
		}

		$pageID = get_option( 'page_on_front' );
		if ( $post && $pageID == $post->ID ) {
			$wp_query->is_home     = true;
			$wp_query->is_single   = false;
			$wp_query->is_singular = false;
		}
		if ( $post && $post->post_type === 'page' && ! $wp_query->is_home ) {
			$wp_query->is_page = true;
		}

		echo wp_kses_post( lana_breadcrumb( $lana_breadcrumb_options ) );

		//restore wp_query flags
		foreach ( $modifiedFlags as $flag ) {
			$wp_query->{$flag} = $backup_flags[ $flag ];
		}
		?>
	</div>
	<?php

	$breadcrumb = ob_get_clean();

	ob_start();

	$breadcrumb_selector = '#' . $atts['id'];

	?>
	<style type="text/css">
		/* breadcrumb separator symbol */
		<?php echo esc_html( $breadcrumb_selector ); ?>	.kubio-breadcrumb > li + li:before {
			content: "<?php echo esc_attr( $breadcrumb_separator ); ?>";
			white-space: pre;
		}
	</style>

	<?php

	$style      = ob_get_clean();
	$breadcrumb = $style . $breadcrumb;

	return "<div id='" . esc_attr( $atts['id'] ) . "' class='breadcrumb-wrapper'>{$breadcrumb}</div>";

}
