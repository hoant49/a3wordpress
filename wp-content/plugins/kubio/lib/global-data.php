<?php


use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\Core\Registry;
use Kubio\Core\StyleManager\GlobalStyleRender;
use Kubio\Core\StyleManager\StyleManager;

function kubio_global_style_post_type() {
	return 'kubio-globals';
}

/**
 * Registers a Custom Post Type to store the user's origin config.
 */
function kubio_register_global_style_post() {
	$args = array(
		'label'        => __( 'Kubio Globals', 'kubio' ),
		'public'       => false,
		'show_ui'      => false,
		'show_in_rest' => true,
		'rest_base'    => 'kubio/global-styles',
		'capabilities' => array(
			'read'                   => 'edit_theme_options',
			'create_posts'           => 'edit_theme_options',
			'edit_posts'             => 'edit_theme_options',
			'edit_published_posts'   => 'edit_theme_options',
			'delete_published_posts' => 'edit_theme_options',
			'edit_others_posts'      => 'edit_theme_options',
			'delete_others_posts'    => 'edit_theme_options',
		),
		'map_meta_cap' => true,
		'supports'     => array(
			'editor',
			'revisions',
		),
	);
	register_post_type( kubio_global_style_post_type(), $args );
	register_post_meta(
		kubio_global_style_post_type(),
		'compiled_css',
		array(
			'show_in_rest'  => true,
			'single'        => true,
			'type'          => 'string',
			'auth_callback' => function () {
				return current_user_can( 'edit_theme_options' );
			},
		)
	);
}


add_action( 'init', 'kubio_register_global_style_post' );


function kubio_global_data_post_id( $create_new = true ) {

	if ( $cached = wp_cache_get( 'id', 'kubio/global_data' ) ) {
		return $cached;
	}

	$post_type = kubio_global_style_post_type();
	$query     = new WP_Query(
		array(
			'post_type'     => $post_type,
			'post_status'   => array( 'draft', 'publish' ),
			'no_found_rows' => true,
			'post_per_page' => 1,
		)
	);

	if ( $query->have_posts() ) {
		$post                      = $query->next_post();
		$kubio_global_post_content = json_decode( $post->post_content, true );
		wp_cache_set( 'data', $kubio_global_post_content, 'kubio/global_data' );
		$id = $post->ID;
	} else {

		if ( $create_new ) {
			$content = file_get_contents( __DIR__ . '/../defaults/global-data.json' );
			$id      = wp_insert_post(
				array(
					'post_content' => json_encode( json_decode( $content, true ) ), // remove the pretty prints
					'post_status'  => 'publish',
					'post_type'    => $post_type,
					'post_name'    => $post_type,
					'post_title'   => __( 'Kubio Globals', 'kubio' ),
				),
				true
			);
		} else {
			return null;
		}
	}

	if ( kubio_is_page_preview() ) {
		$autosaved_posts = kubio_get_current_changeset_data( 'autosaves', array() );

		foreach ( $autosaved_posts as $autosaved_post ) {
			$autosaved_parent = intval( Arr::get( $autosaved_post, 'parent', 0 ) );
			if ( $autosaved_parent === intval( $id ) ) {
				return $autosaved_post['id'];
			}
		}
	}

	wp_cache_set( 'id', $id, 'kubio/global_data' );

	return $id;
}

function kubio_get_global_data_content( $redo_cache = false ) {

	if ( ! $redo_cache && $cached = wp_cache_get( 'data', 'kubio/global_data' ) ) {
		return $cached;
	}

	$id                        = kubio_global_data_post_id();
	$post                      = get_post( $id );
	$kubio_global_post_content = json_decode( $post->post_content, true );
	wp_cache_set( 'data', $kubio_global_post_content, 'kubio/global_data' );

	return $kubio_global_post_content;
}

function kubio_get_global_data( $path, $fallback = null ) {
	$data = kubio_get_global_data_content();

	return Arr::get( $data, $path, $fallback );
}

function kubio_replace_global_data_content( $data ) {
	wp_update_post(
		array(
			'ID'           => kubio_global_data_post_id(),
			'post_content' => json_encode( $data ),
		)
	);
}

function kubio_set_global_data( $path, $value ) {
	$data = kubio_get_global_data_content();
	$data = Arr::set( $data, $path, $value );

	wp_cache_set( 'data', $data, 'kubio/global_data' );
	kubio_replace_global_data_content( $data );
}


function kubio_edit_global_styles_editor_settings( $settings ) {

	$settings['kubioGlobalStyleEntityType'] = kubio_global_style_post_type();
	$settings['kubioGlobalStyleEntityId']   = kubio_global_data_post_id();
	$settings['kubioGlobalStyleDefaults']   = kubio_get_global_data_content();

	return $settings;
}

function kubio_nav_menu_locations_from_global_data( $locations ) {
	$global_data_locations = kubio_get_global_data( 'menuLocations', array() );

	$should_update = false;
	foreach ( $global_data_locations as $global_data_location ) {
		$location = $global_data_location['name'];
		$menu_id  = $global_data_location['menu'];

		if ( $menu_id ) {
			if ( ! isset( $locations[ $location ] ) || $locations[ $location ] !== $menu_id ) {
				$should_update = true;
			}
			$locations[ $location ] = $menu_id;
		}
	}

	if ( $should_update && ! kubio_is_page_preview() ) {
		set_theme_mod( 'nav_menu_locations', $locations );
	}

	return $locations;
}

add_filter(
	'theme_mod_nav_menu_locations',
	'kubio_nav_menu_locations_from_global_data'
);

add_filter( 'block_editor_settings_all', 'kubio_edit_global_styles_editor_settings' );

function kubio_register_global_style() {
	$styles = kubio_get_global_data_content();
	$styles = Arr::get( $styles, 'globalStyle', array() );

	$styleRenderer = new GlobalStyleRender( $styles );
	$globalStyle   = $styleRenderer->export();

	StyleManager::getInstance()->registerBlockStyle( $globalStyle );
}

function kubio_render_global_colors() {
	$styles                 = kubio_get_global_data_content();
	list( $color_palette, ) = (array) get_theme_support( 'editor-color-palette' );
	$color_palette          = Arr::get( $styles, 'colors', $color_palette );

	$vars          = array();
	$color_palette = is_array( $color_palette ) ? $color_palette : array();
	foreach ( $color_palette as $index => $value ) {
		$vars[] = '--' . $value['slug'] . ':' . implode( ',', $value['color'] );
	}

	$color_palette_variants = Arr::get( $styles, 'colorVariants', $color_palette );
	foreach ( $color_palette_variants as $index => $value ) {
		$vars[] = '--' . $value['slug'] . ':' . implode( ',', $value['color'] );
	}

	$new_line = ( defined( 'KUBIO_DEBUG' ) && KUBIO_DEBUG ) ? "\n" : '';
	$css      = array( ':root {' );
	$css[]    = implode( ";{$new_line}", $vars );
	$css[]    = '}';

	foreach ( $color_palette as $value ) {
		// has-kubio-color-2-color has-kubio-color-1-background-color
		$css[] = ".has-{$value['slug']}-color{color:rgb(var(--{$value['slug']}))}";
		$css[] = ".has-{$value['slug']}-background-color{background-color:rgb(var(--{$value['slug']}))}";
	}

	return implode( $new_line, $css );
}


add_filter(
	'kubio/google_fonts',
	function ( $fonts ) {
		$styles    = kubio_get_global_data_content();
		$new_fonts = Arr::get( $styles, 'fonts.google', array() );

		return array_merge( $fonts, $new_fonts );
	}
);

function kubio_enqueue_google_fonts() {
	$fonts = array();
	$fonts = apply_filters( 'kubio/google_fonts', $fonts );

	$renderedFonts = Registry::getInstance()->getRenderedFonts();
	foreach ( $renderedFonts as $family => $variants ) {
		$fonts[] = array(
			'family'   => $family,
			'variants' => $variants,
		);
	}
	if ( ! count( $fonts ) ) {
		return;
	}

	$fontQuery = array();
	foreach ( $fonts as $font ) {
		$variants    = Arr::get( $font, 'variants', array() );
		$variants    = array_filter(
			$variants,
			function ( $variant ) {
				return ! empty( $variant );
			}
		);
		$fontQuery[] = $font['family'] . ':' . implode( ',', empty( $variants ) ? array( 400 ) : $variants );
	}

	$query_args = array(
		'family'  => urlencode( implode( '|', $fontQuery ) ),
		'subset'  => urlencode( 'latin,latin-ext' ),
		'display' => 'swap',
	);

	$fontsURL = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	wp_enqueue_style( 'kubio-google-fonts', $fontsURL, array(), null );
}

add_action( 'wp_enqueue_scripts', 'kubio_enqueue_google_fonts' );

function kubio_enqueue_typekit_fonts() {
	$globalData     = kubio_get_global_data_content();
	$typeKitProject = Arr::get( $globalData, 'fonts.typekit.project', '' );

	if ( ! empty( $typeKitProject ) ) {
		?>
		<script>
			(function (d) {
				var config = {
						kitId: '<?php echo esc_js( $typeKitProject ); ?>',
						scriptTimeout: 3000,
						async: true
					},
					h = d.documentElement,
					t = setTimeout(function () {
						h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
					}, config.scriptTimeout),
					tk = d.createElement("script"),
					f = false,
					s = d.getElementsByTagName("script")[0],
					a;
				h.className += " wf-loading";
				tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
				tk.async = true;
				tk.onload = tk.onreadystatechange = function () {
					a = this.readyState;
					if (f || a && a != "complete" && a != "loaded") return;
					f = true;
					clearTimeout(t);
					try {
						Typekit.load(config)
					} catch (e) {
					}
				};
				s.parentNode.insertBefore(tk, s)
			})(document);
		</script>
		<?php
	}
}

add_action( 'wp_head', 'kubio_enqueue_typekit_fonts' );


add_action(
	'admin_init',
	function () {

		$styles = kubio_get_global_data_content();
		global $_wp_registered_theme_features;

		if ( isset( $_wp_registered_theme_features['editor-color-palette'] ) ) {
			unset( $_wp_registered_theme_features['editor-color-palette'] );
		}

		$color_palette = Arr::get( $styles, 'colors', array() );

		foreach ( $color_palette as $index => $value ) {
			if ( ! is_array( $value['color'] ) ) {
				continue;
			}
			$color_palette[ $index ] = array_merge(
				$color_palette[ $index ],
				array(
					'color' => 'rgb(' . implode( ',', $value['color'] ) . ')',
				)
			);
		}
		add_theme_support( 'editor-color-palette', $color_palette );
	},
	20
);

add_action( 'wp_head', 'kubio_register_global_style', 0 );


add_filter(
	'rest_prepare_' . kubio_global_style_post_type(),
	function ( $response, $post ) {
		$response->set_data(
			array_merge(
				$response->get_data(),
				array(
					'parsed' => json_decode( $post->post_content ),
				)
			)
		);

		return $response;
	},
	10,
	2
);
