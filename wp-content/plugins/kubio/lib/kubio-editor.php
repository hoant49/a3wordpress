<?php

use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\Core\Utils;

function kubio_is_kubio_editor_page() {
	 global $pagenow;

	if ( substr( $pagenow, 0, -4 ) === 'admin' ) {
		$page = Arr::get( $_REQUEST, 'page', false );

		if ( $page === 'kubio' ) {
			return true;
		}
	}
}

function kubio_edit_site_get_first_post_id( $loaded_id ) {
	if ( ! $loaded_id ) {
		$ids = get_posts(
			array(
				'post_type'      => 'page',
				'posts_per_page' => 1,
				'fields'         => 'ids',
				'orderby'        => 'date',
				'order'          => 'ASC',
			)
		);

		if ( ! empty( $ids ) ) {
			$loaded_id = $ids[0];
		}
	}

	if ( ! $loaded_id ) {
		$ids = get_posts(
			array(
				'post_type'      => 'post',
				'posts_per_page' => 1,
				'fields'         => 'ids',
			)
		);

		if ( ! empty( $ids ) ) {
			$loaded_id = $ids[0];
		}
	}
	return $loaded_id;
}
//this should also treat the template parts case. But at the moment you can't preview template parts but if will in the future
//the logic should work the same
function kubio_edit_site_get_template_id( $loaded_id, $postType ) {

	$parts = explode( '//', $loaded_id );
	if ( count( $parts ) !== 2 ) {
		return null;
	}
	$templateName = $parts[1];
	$query        = array(
		'name'           => $templateName,
		'post_type'      => $postType,
		'fields'         => 'ids',
		'post_status'    => 'publish',
		'posts_per_page' => 1,
	);
	$ids          = get_posts( $query );
	$resultPostId = null;
	if ( ! empty( $ids ) ) {
		$resultPostId = $ids[0];
	}
	return $resultPostId;
}
function kubio_edit_site_get_edited_entity() {
	$pag_on_front = intval( get_option( 'page_on_front' ) );
	$loaded_id    = isset( $_GET['postId'] ) ? intval( $_GET['postId'] ) : $pag_on_front;
	$post_type    = isset( $_GET['postType'] ) ? sanitize_text_field( $_GET['postType'] ) : null;

	if ( ! post_type_exists( $post_type ) || ! is_numeric( $loaded_id ) ) {
		return array();
	}

	//when you have latest post set as your home page and you enter the builder without the post id the loaded_id will be 0
	//in this case we'll load the index template
	if ( $loaded_id === 0 ) {
		$themeName       = get_stylesheet();
		$indexTemplateId = strtr( ':theme//index', array( ':theme' => $themeName ) );
		$loaded_id       = $indexTemplateId;
		$post_type       = 'wp_template';
	}

	$templatePostId = null;
	if ( ! ! $loaded_id && ! is_numeric( $loaded_id ) ) {
		$templatePostId = $loaded_id;
		$loaded_id      = kubio_edit_site_get_template_id( $loaded_id, $post_type );
	}
	if ( ! $loaded_id || ! is_numeric( $loaded_id ) ) {
		$loaded_id = kubio_edit_site_get_first_post_id( $loaded_id );
	}

	if ( $loaded_id ) {
		$entity = get_post( $loaded_id );

		return array(
			'path'    => get_permalink( $loaded_id ),
			'context' => array(
				'postType'  => $entity ? $entity->post_type : '',
				'postId'    => $templatePostId ? $templatePostId : $loaded_id,
				'query'     => '',
				'postTitle' => $entity ? $entity->post_title : '',
				'title'     => $entity ? $entity->post_title : '',
			),
			'slug'    => $entity ? $entity->post_name : '',
			'label'   => $entity ? $entity->post_title : '',
		);
	}

	return array();
}


function kubio_get_patterns_categories() {
	return array(
		array(
			'label' => __( 'Hero accent', 'kubio' ),
			'name'  => 'kubio-content/overlappable',
		),

		array(
			'label' => __( 'About', 'kubio' ),
			'name'  => 'kubio-content/about',
		),

		array(
			'label' => __( 'Features', 'kubio' ),
			'name'  => 'kubio-content/features',
		),

		array(
			'label' => __( 'Content', 'kubio' ),
			'name'  => 'kubio-content/content',
		),

		array(
			'label' => __( 'Call to action', 'kubio' ),
			'name'  => 'kubio-content/cta',
		),
		array(
			'label' => __( 'Blog', 'kubio' ),
			'name'  => 'kubio-content/blog',
		),

		array(
			'label' => __( 'Counters', 'kubio' ),
			'name'  => 'kubio-content/counters',
		),

		array(
			'label' => __( 'Portfolio', 'kubio' ),
			'name'  => 'kubio-content/portfolio',
		),

		array(
			'label' => __( 'Photo gallery', 'kubio' ),
			'name'  => 'kubio-content/photo gallery',
		),

		array(
			'label' => __( 'Testimonials', 'kubio' ),
			'name'  => 'kubio-content/testimonials',
		),

		array(
			'label' => __( 'Clients', 'kubio' ),
			'name'  => 'kubio-content/clients',
		),

		array(
			'label' => __( 'Team', 'kubio' ),
			'name'  => 'kubio-content/team',
		),

		array(
			'label' => __( 'Contact', 'kubio' ),
			'name'  => 'kubio-content/contact',
		),

		array(
			'label' => __( 'F.A.Q.', 'kubio' ),
			'name'  => 'kubio-content/f.a.q.',
		),

		array(
			'label' => __( 'Pricing', 'kubio' ),
			'name'  => 'kubio-content/pricing',
		),

		array(
			'label' => __( 'Headers', 'kubio' ),
			'name'  => 'kubio-header/headers',
		),

		array(
			'label' => __( 'Footers', 'kubio' ),
			'name'  => 'kubio-footer/footers',
		),
	);
}

function kubio_extend_block_editor_styles_html() {
	$style_handles = array(
		'wp-edit-blocks',
		'wp-block-editor',
		'wp-block-library',
		'kubio-editor',
		'kubio-controls',
		'kubio-format-library',
		'kubio-pro',
		'kubio-block-library-editor',
		'kubio-third-party-blocks',
	);

	$block_registry = WP_Block_Type_Registry::get_instance();

	foreach ( $block_registry->get_all_registered() as $block_type ) {
		if ( ! empty( $block_type->style ) ) {
			$style_handles[] = $block_type->style;
		}

		if ( ! empty( $block_type->editor_style ) ) {
			$style_handles[] = $block_type->editor_style;
		}
	}

	$style_handles = array_unique( $style_handles );
	$done          = wp_styles()->done;

	wp_add_inline_style( 'common', 'body {background-color:#fff}' );
	ob_start();
	wp_styles()->done = array();
	wp_styles()->do_items( $style_handles );
	wp_styles()->done = $done;

	$style = ob_get_clean();

	$script_handles = array( 'kubio-frontend' );

	$script = '';

	$done = wp_scripts()->done;
	ob_start();
	wp_scripts()->done = array();
	wp_scripts()->do_items( $script_handles );
	$script            = ob_get_clean();
	wp_scripts()->done = $done;

	$script = "<template id='kubio-scripts-template'>{$script}</template>";

	printf(
		'<script>window.__kubioEditorStyles = %s</script>',
		wp_json_encode( array( 'html' => $style . $script ) )
	);
}

function kubio_edit_site_get_settings() {
	$max_upload_size = wp_max_upload_size();
	if ( ! $max_upload_size ) {
		$max_upload_size = 0;
	}

	// This filter is documented in wp-admin/includes/media.php.
	// This filter is documented in wp-admin/includes/media.php.
	$image_size_names      = apply_filters(
		'image_size_names_choose',
		array(
			'thumbnail' => __( 'Thumbnail', 'kubio' ),
			'medium'    => __( 'Medium', 'kubio' ),
			'large'     => __( 'Large', 'kubio' ),
			'full'      => __( 'Full Size', 'kubio' ),
		)
	);
	$available_image_sizes = array();
	foreach ( $image_size_names as $image_size_slug => $image_size_name ) {
		$available_image_sizes[] = array(
			'slug' => $image_size_slug,
			'name' => $image_size_name,
		);
	}

	$settings = array_merge(
		get_default_block_editor_settings(),
		array(
			'alignWide'                            => get_theme_support( 'align-wide' ),
			'siteUrl'                              => site_url( '/' ),
			'postsPerPage'                         => get_option( 'posts_per_page' ),
			'defaultTemplateTypes'                 => kubio_get_indexed_default_template_types(),
			'__experimentalBlockPatterns'          => WP_Block_Patterns_Registry::get_instance()->get_all_registered(),
			'__experimentalBlockPatternCategories' => array_merge(
				WP_Block_Pattern_Categories_Registry::get_instance()->get_all_registered(),
				kubio_get_patterns_categories()
			),
			'imageSizes'                           => $available_image_sizes,
			'isRTL'                                => is_rtl(),
			'maxUploadFileSize'                    => $max_upload_size,
		)
	);

	if ( function_exists( 'gutenberg_experimental_global_styles_settings' ) ) {
		$settings = gutenberg_experimental_global_styles_settings( $settings );
	}

	$settings['state'] = array(
		'entity' => kubio_edit_site_get_edited_entity(),
	);

	$settings['kubioPrimaryMenuLocation'] = apply_filters(
		'kubio/primary_menu_location',
		'header-menu'
	);

	$settings['supportsTemplateMode'] = kubio_theme_has_block_templates_support();

	global $post;
	$settings                  = apply_filters( 'block_editor_settings_all', $settings, $post );
	$settings                  = apply_filters( 'kubio/block_editor_settings', $settings, $post );
	$settings['isWooCommerce'] = function_exists( 'WC' );

	$settings['defaultTemplatePartAreas'] = kubio_get_allowed_template_part_areas();

	return $settings;
}

add_filter(
	'block_editor_settings_all',
	function ( $settings ) {
		$settings['__unstableEnableFullSiteEditingBlocks'] = true;
		$settings['enableFSEBlocks']                       = true;

		return $settings;
	},
	50
);

function kubio_load_gutenberg_assets() {
	wp_enqueue_style( 'kubio-pro' );

	wp_enqueue_script( 'kubio-block-patterns' );
	wp_enqueue_script( 'kubio-third-party-blocks' );
	wp_localize_script(
		'kubio-block-patterns',
		'kubioBlockPatterns',
		array(
			'inGutenbergEditor' => ! kubio_is_kubio_editor_page(),
		)
	);

	wp_add_inline_style( 'kubio-block-library', kubio_get_shapes_css() );
}

function kubio_get_post_types() {
	$extra_types = get_post_types(
		array(
			'_builtin' => false,
			'public'   => true,
		)
	);

	$types = array();
	global $wp_post_types;

	foreach ( $extra_types as $type ) {
		$types[] = array(
			'entity' => $type,
			'kind'   => 'postType',
			'title'  => $wp_post_types[ $type ]->label,
		);
	}

	return $types;
}

add_action( 'enqueue_block_editor_assets', 'kubio_load_gutenberg_assets' );

function kubio_navigation_get_menu_items_endpoint(
	$menu_id,
	$results_per_page = 100
) {
	return '/__experimental/menu-items?' .
		build_query(
			array(
				'context'  => 'edit',
				'menus'    => $menu_id,
				'per_page' => $results_per_page,
				'_locale'  => 'user',
			)
		);
}

function kubio_add_endpoint_details(
	$url,
	$results_per_page = null,
	$locale = null
) {
	$vars = array( 'context' => 'edit' );
	if ( $results_per_page !== null ) {
		$vars['per_page'] = $results_per_page;
	}
	if ( $locale !== null ) {
		$vars['_locale'] = 'user';
	}
	return $url . '?' . build_query( $vars );
}

function kubio_edit_site_init( $hook ) {
	if ( ! kubio_is_kubio_editor_page() ) {
		return;
	}
	add_filter( 'use_block_editor_for_post_type', '__return_true' );

	global $current_screen, $post;
	$current_screen->is_block_editor( true );

	// Inline the Editor Settings
	$settings = kubio_edit_site_get_settings();

	$menus         = wp_get_nav_menus();
	$first_menu_id = ! empty( $menus ) ? $menus[0]->term_id : null;

	$stylesheet = get_stylesheet();

	// Preload block editor paths.
	// most of these are copied from edit-forms-blocks.php.

	$preload_paths = array(
		array( '/wp/v2/media', 'OPTIONS' ),
		'/',
		'/?context=edit',
		'/wp/v2/types?context=edit',
		'/wp/v2/taxonomies?context=edit',
		'/wp/v2/categories?context=edit',
		'/wp/v2/categories/1?context=edit',
		'/wp/v2/tags?context=edit',

		'/wp/v2/settings',
		'/wp/v2/themes?status=active',
		'/wp/v2/types?context=edit',

		array( '/wp/v2/pages', 'OPTIONS' ),
		array( '/wp/v2/posts', 'OPTIONS' ),

		kubio_add_endpoint_details( '/wp/v2/pages', 100 ),
		kubio_add_endpoint_details( '/wp/v2/posts', 100 ),
		kubio_add_endpoint_details( '/wp/v2/templates', 100 ),
		kubio_add_endpoint_details( '/wp/v2/template-parts', 100 ),

		'/kubio/v1/contact-form/forms_by_type',
		'/kubio/v1/subscribe-form/forms_by_type',
		'/kubio/v1/page-templates/get',

		'/__experimental/menus?_locale=user&context=edit&per_page=100',
		'/__experimental/menus?context=edit&per_page=-1',
		'/__experimental/menu-locations?context=edit',

		kubio_add_endpoint_details( "/wp/v2/template-parts/{$stylesheet}//footer" ),
		kubio_add_endpoint_details( "/wp/v2/template-parts/{$stylesheet}//front-header" ),
		kubio_add_endpoint_details( "/wp/v2/templates/kub{$stylesheet}io//front-page" ),
	);

	wp_enqueue_media();

	$preload_paths[] =
		'/wp/v2/kubio/global-styles/' .
		kubio_global_data_post_id() .
		'?context=edit';

	$preload_paths[] = '/kubio/v1/menu/';
	$preload_paths[] =
		'/wp/v2/pages/' . get_option( 'page_on_front' ) . '?context=edit';

	$preload_paths[] = '/wp/v2/users/1?context=edit';
	$preload_paths[] = '/wp/v2/comments?context=edit&post=1';

	if ( $first_menu_id ) {
		$preload_paths[] = "/kubio/v1/menu/${first_menu_id}";
		$preload_paths[] = kubio_navigation_get_menu_items_endpoint(
			$first_menu_id
		);
	}

	$preload_data = array_reduce(
		$preload_paths,
		'rest_preload_api_request',
		array()
	);

	wp_add_inline_script(
		'wp-api-fetch',
		sprintf(
			'wp.apiFetch.use( wp.apiFetch.createPreloadingMiddleware( %s ) );',
			wp_json_encode( $preload_data )
		),
		'after'
	);

	wp_add_inline_script(
		'wp-blocks',
		sprintf(
			'wp.blocks.unstable__bootstrapServerSideBlockDefinitions( %s );',
			wp_json_encode( get_block_editor_server_block_settings() )
		),
		'after'
	);

	wp_add_inline_script(
		'wp-blocks',
		sprintf(
			'wp.blocks.setCategories( %s );',
			wp_json_encode( get_block_categories( $post ) )
		),
		'after'
	);

	// Editor default styles
	wp_enqueue_style( 'wp-format-library' );
	wp_enqueue_style( 'kubio-format-library' );
	wp_enqueue_script( 'kubio-editor' );
	wp_enqueue_script( 'kubio-block-library' );
	wp_enqueue_style( 'kubio-admin-panel' );

	wp_enqueue_style( 'kubio-editor' );
	wp_enqueue_style( 'kubio-advanced-panel' );
	wp_enqueue_style( 'kubio-block-library-editor' );
	wp_enqueue_style( 'kubio-controls' );
	wp_enqueue_style( 'kubio-scripts' );

	//
	wp_enqueue_script( 'wp-tinymce' );
	wp_enqueue_editor();
	wp_enqueue_code_editor( array() );
	wp_tinymce_inline_scripts();

	$settings['postTypes'] = kubio_get_post_types();

	wp_add_inline_script(
		'kubio-editor',
		sprintf(
			'window.kubioEditSiteSettings = %s;' .
				"\n" .
				'wp.domReady( function() { kubio.editor.initialize( "kubio-editor", window.kubioEditSiteSettings );	} );',
			wp_json_encode( $settings )
		),
		'after'
	);

	wp_add_inline_style( 'kubio-editor', kubio_get_shapes_css() );

	add_action( 'admin_footer', 'kubio_extend_block_editor_styles_html' );

	do_action( 'enqueue_block_editor_assets' );
	do_action( 'kubio/editor/enqueue_assets' );
}

add_action( 'admin_enqueue_scripts', 'kubio_edit_site_init', 500 );

/**
 * Renders the Menu Page
 *
 * @return void
 */
function kubio_edit_site_render_block_editor() {    ?>
	<style>
		div#wpcontent,
		#wpbody-content {
			position: fixed;
			z-index: 1000000;
			width: 100vw;
			height: 100vh;
			background: #fff;
			top: 0%;
			left: 0%;
			right: 0;
			border: 0;
			display: block;
			box-sizing: border-box;
		}
		#wp-link-backdrop {
			z-index: 1000050;
		}
		#wp-link-wrap {
			z-index: 1000100;
		}
		#kubio-editor {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			z-index: 1000;
			width: 100%;
			height: 100%;
		}

		span.kubio-loading-editor-text {
			position: absolute;
			top: 50%;
			left: 50%;
			display: block;
			font-size: 1.5em;
			transform: translate(-50%, -50%);
		}
	</style>
	<div id="kubio-editor" class="kubio">
		<span class="kubio-loading-editor-text"><?php esc_html_e( 'Loading Editor&hellip;', 'kubio' ); ?></span>
	</div>
	<?php
}

/**
 * Registers the new WP Admin Menu
 *
 * @return void
 */
function kubio_edit_site_add_menu_page() {
	add_menu_page(
		__( 'Kubio', 'kubio' ),
		sprintf(
			'<span class="kubio-menu-item"><span class="kubio-menu-item--icon">%s</span><span>%s</span></span>',
			wp_kses_post( KUBIO_LOGO_SVG ),
			__( 'Kubio', 'kubio' )
		),
		'edit_posts',
		'kubio',
		'kubio_edit_site_render_block_editor',
		'data:image/svg+xml;base64,' . base64_encode( str_replace( '<svg', '<svg fill="#5246F1" ', KUBIO_LOGO_SVG ) ),
		20
	);

	add_submenu_page(
		'themes.php',
		__( 'Kubio', 'kubio' ),
		__( 'Edit with Kubio', 'kubio' ),
		'edit_posts',
		'kubio_appearance_edit',
		'kubio_edit_site_render_block_editor',
		1
	);

	global $submenu;

	foreach ( $submenu['themes.php'] as $index => $submenu_item ) {
		if ( $submenu_item[2] === 'kubio_appearance_edit' ) {
			$submenu['themes.php'][ $index ][2] = add_query_arg(
				array(
					'page' => 'kubio',
				),
				'admin.php'
			);
		}
	}
}

function kubio_admin_page_submenu_hook() {
	global $submenu;

	$kubio_submenu = Arr::get( $submenu, 'kubio', null );

	if ( is_array( $kubio_submenu ) ) {
		foreach ( $kubio_submenu as $index => $submenu_data ) {
			$menu_slug = Arr::get( $submenu_data, '2', null );
			if ( $menu_slug === 'kubio' ) {
				$submenu['kubio'][ $index ][0] = __( 'Open Kubio Editor', 'kubio' );
			}
		}
	}

}

add_action( 'admin_menu', 'kubio_admin_page_submenu_hook', 200 );

add_action( 'admin_menu', 'kubio_edit_site_add_menu_page' );

function kubio_admin_menu_style() {
	?>
	<style>
		#adminmenu .toplevel_page_kubio .wp-menu-image {
			display: none;
		}

		.wp-admin.folded #adminmenu .toplevel_page_kubio .wp-menu-image{
			display:block;
			background-size: 16px auto;
		}

		#adminmenu .kubio-menu-item {
			display: flex;
			align-items: center;
		}

		#adminmenu .kubio-menu-item > span {
			display: block;
			line-height: 1;
		}

		#adminmenu .kubio-menu-item--icon {
			color: rgba(240,246,252,.6);
		}
		#adminmenu .wp-menu-name:hover .kubio-menu-item--icon {
			color: inherit;
		}
		#adminmenu .kubio-menu-item--icon svg {
			fill: currentColor;
			height: 15px;
			margin-right: 11px;
		}

		#adminmenu .wp-menu-open .kubio-menu-item--icon svg {
			fill: #fff;
		}

		#adminmenu .toplevel_page_kubio div.wp-menu-name {
			padding: 8px 8px 8px 10px;
		}

		#adminmenu .wp-submenu .kubio-menu-item--icon {
			display: none;
		}
	</style>
	<?php
}

add_action( 'admin_head', 'kubio_admin_menu_style' );

/**
 * Register a core site setting for front page information.
 */
function kubio_register_site_editor_homepage_settings() {
	global $wp_registered_settings;

	if ( ! isset( $wp_registered_settings['show_on_front'] ) ) {
		register_setting(
			'reading',
			'show_on_front',
			array(
				'show_in_rest' => true,
				'type'         => 'string',
				'description'  => __( 'What to show on the front page', 'kubio' ),
			)
		);
	}

	if ( ! isset( $wp_registered_settings['page_on_front'] ) ) {
		register_setting(
			'reading',
			'page_on_front',
			array(
				'show_in_rest' => true,
				'type'         => 'number',
				'description'  => __(
					'The ID of the page that should be displayed on the front page',
					'kubio'
				),
			)
		);
	}
	if ( ! isset( $wp_registered_settings['page_for_posts'] ) ) {
		register_setting(
			'reading',
			'page_for_posts',
			array(
				'show_in_rest' => true,
				'type'         => 'number',
				'description'  => __(
					'The ID of the page that displays the posts',
					'kubio'
				),
			)
		);
	}

	if ( ! isset( $wp_registered_settings['site_icon'] ) ) {
		register_setting(
			'general',
			'site_icon',
			array(
				'show_in_rest' => array(
					'name' => 'site_icon',
				),
				'type'         => 'integer',
				'description'  => __( 'Site icon.', 'kubio' ),
			)
		);
	}
}

add_filter(
	'block_editor_settings_all',
	function ( $settings ) {
		$settings['kubioLoaded'] = true;

		return $settings;
	}
);

add_action( 'init', 'kubio_register_site_editor_homepage_settings', 100 );
