<?php

use IlluminateAgnostic\Arr\Support\Arr;

if ( ! defined( 'KUBIO_HYBRID_THEME_EDITOR_QUERY_PARAM' ) ) {
	define( 'KUBIO_HYBRID_THEME_EDITOR_QUERY_PARAM', '__kubio-site-edit-iframe-preview' );
}

function kubio_is_hybdrid_theme_iframe() {
	return Arr::has( $_REQUEST, '__kubio-site-edit-iframe-preview' );
}


require_once __DIR__ . '/hybrid-theme/templates.php';
require_once __DIR__ . '/hybrid-theme/editor-hooks.php';
require_once __DIR__ . '/hybrid-theme/frontend-hooks.php';
require_once __DIR__ . '/hybrid-theme/customizer-options.php';

