<?php

do_action( 'kubio/before-load-full-width-template' );

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

?>
<div id="kubio-full-width-container" class="<?php echo esc_attr( kubio_get_kubio_full_width_container_class() ); ?>">
	<?php
	while ( have_posts() ) :
		the_post();
		the_content();
	endwhile;
	?>
</div>
<?php

get_footer();
