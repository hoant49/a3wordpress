<?php


use Kubio\Core\EditInKubioCustomizerPanel;

function kubio_add_kubio_forced_full_width_class_toggle( $wp_customize ) {

	if ( kubio_theme_has_block_templates_support() ) {
		$wp_customize->add_panel(
			new EditInKubioCustomizerPanel(
				$wp_customize,
				'kubio-edit-in-kubio-section',
				array(
					'capability' => 'manage_options',
					'priority'   => 0,
				)
			)
		);

		return;
	}

	$wp_customize->add_section(
		'kubio-full-width-template',
		array(
			'title'    => __( 'Kubio full width template', 'kubio' ),
			'priority' => PHP_INT_MAX,
		)
	);

	$wp_customize->add_setting(
		'kubio-full-width-template-background-color',
		array(
			'default'    => '#ffffff',
			'type'       => 'option',
			'transport'  => 'refresh',
			'capability' => 'edit_theme_options',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'kubio-full-width-template-background-color',
			array(
				'label'    => __( 'Background Color', 'kubio' ),
				'section'  => 'kubio-full-width-template',
				'settings' => 'kubio-full-width-template-background-color',
			)
		)
	);

	$wp_customize->add_setting(
		'kubio-full-width-container-forced',
		array(
			'default'    => false,
			'type'       => 'option',
			'transport'  => 'refresh',
			'capability' => 'edit_theme_options',
		)
	);

	$wp_customize->add_control(
		'kubio-full-width-container-forced',
		array(
			'label'    => __( 'Force full width display', 'kubio' ),
			'section'  => 'kubio-full-width-template',
			'settings' => 'kubio-full-width-container-forced',
			'type'     => 'checkbox',
			'priority' => 100,
		)
	);

}

add_action( 'customize_register', 'kubio_add_kubio_forced_full_width_class_toggle' );
