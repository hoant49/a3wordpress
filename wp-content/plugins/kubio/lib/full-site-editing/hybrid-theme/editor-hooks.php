<?php

function kubio_hybrid_theme_post_content_placeholder() {
	return '<div id="kubio-site-edit-content-holder"></div>';
}

function kubio_hybrid_theme_assets_holder() {
	?>
	<template id="kubio-site-edit-assets-holder"></template>
	<?php
}


function kubio_hybrid_theme_iframe_hide_admin_bar() {
	show_admin_bar( false );
}

if ( kubio_is_hybdrid_theme_iframe() ) {
	add_filter( 'the_content', 'kubio_hybrid_theme_post_content_placeholder', 0, 1 );
	add_action( 'wp_head', 'kubio_hybrid_theme_assets_holder', PHP_INT_MAX );
	add_action( 'after_setup_theme', 'kubio_hybrid_theme_iframe_hide_admin_bar' );
}

function kubio_wp_redirect_maybe_add_hybrid_theme_query_param( $location ) {

	if ( kubio_is_hybdrid_theme_iframe() ) {
		$location = add_query_arg( KUBIO_HYBRID_THEME_EDITOR_QUERY_PARAM, 'true', $location );
	}

	return $location;

}

add_filter( 'wp_redirect', 'kubio_wp_redirect_maybe_add_hybrid_theme_query_param' );
