<?php

function kubio_hybdrid_theme_classic_content_frontend_style( $content ) {

	if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) {
		return $content;
	}

	if ( ! kubio_is_block_template() ) {
		return sprintf(
			'<!-- content style : start -->%s<!-- content style : end -->%s',
			kubio_render_page_css(),
			$content
		);
	}

	return $content;
}

add_filter( 'the_content', 'kubio_hybdrid_theme_classic_content_frontend_style', PHP_INT_MAX );

add_filter(
	'language_attributes',
	function ( $html_attrs ) {
		$html_attrs .= ' id="kubio"';

		return $html_attrs;
	}
);
