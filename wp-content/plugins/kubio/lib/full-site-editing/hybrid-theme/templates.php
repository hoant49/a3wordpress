<?php

function kubio_hybrid_theme_proposed_templates() {
	return array(
		'kubio-full-width' => _x( 'Kubio Full Width', 'Page Template', 'kubio' ),
		'kubio-canvas'     => _x( 'Kubio Blank Canvas', 'Page Template', 'kubio' ),
	);
}

function kubio_add_page_templates( $page_templates ) {

	$page_templates = kubio_hybrid_theme_proposed_templates() + $page_templates;

	return $page_templates;
}

function kubio_add_classic_theme_proposed_templates() {
	if ( ! kubio_theme_has_block_templates_support() ) {
		add_filter( 'theme_templates', 'kubio_add_page_templates', 10 );
	}
}

add_action( 'init', 'kubio_add_classic_theme_proposed_templates' );


function kubio_classic_theme_template_include( $template ) {
	if ( is_singular() ) {
		// print_r( $template );
		$post_id = get_the_ID();

		$page_template   = get_post_meta( $post_id, '_wp_page_template', true );
		$kubio_templates = array_keys( kubio_hybrid_theme_proposed_templates() );

		if ( in_array( $page_template, $kubio_templates, true ) ) {
			$kubio_template = __DIR__ . "/proposed-templates/{$page_template}.php";
			if ( file_exists( $kubio_template ) ) {
					return $kubio_template;
			} else {
				$tag_templates = array(
					'is_embed'             => 'get_embed_template',
					'is_front_page'        => 'get_front_page_template',
					'is_home'              => 'get_home_template',
					'is_privacy_policy'    => 'get_privacy_policy_template',
					'is_post_type_archive' => 'get_post_type_archive_template',
					'is_attachment'        => 'get_attachment_template',
					'is_single'            => 'get_single_template',
					'is_page'              => 'get_page_template',
					'is_singular'          => 'get_singular_template',
				);

				foreach ( $tag_templates as $tag => $template_getter ) {
					if ( call_user_func( $tag ) ) {
						$template = call_user_func( $template_getter );
						break;
					}
				}

				return $template;
			}
		}
	}

	return $template;

}
add_filter( 'template_include', 'kubio_classic_theme_template_include', 11 /* After Plugins/WooCommerce */ );


function kubio_full_width_template_style() {

	$bg_color = get_theme_mod( 'kubio-full-width-template-background-color', '#ffffff' );

	?>
	<style>
		#kubio-full-width-container {
			background-color: <?php echo esc_html( $bg_color ); ?>;
			min-width: 100%;
		}

		#kubio-full-width-container.kubio-full-width-container-forced{
			width: 100vw;
			position: relative;
			left: 50%;
			right: 50%;
			margin-left: -50vw;
			margin-right: -50vw;
		 }
	</style>
	<?php
}

function kubio_before_load_full_width_template() {
	add_action( 'wp_head', 'kubio_full_width_template_style', 0 );
}

add_action( 'kubio/before-load-full-width-template', 'kubio_before_load_full_width_template' );


function kubio_get_kubio_full_width_container_class( $return_array = false ) {
	$is_forced = get_theme_mod( 'kubio-full-width-container-forced', false );

	$class_list = array();
	if ( ! ! intval( $is_forced ) ) {
		$class_list[] = 'kubio-full-width-container-forced';
	}

	$class_list = apply_filters( 'kubio/kubio-full-width-container-class-list', $class_list );

	if ( $return_array ) {
		return $class_list;
	}

	return implode( ' ', $class_list );
}


