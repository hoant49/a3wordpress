<?php

/** base */

use Kubio\Core\Activation;
use Kubio\DemoSites\DemoSites;

require_once __DIR__ . '/env.php';
require_once __DIR__ . '/filters.php';
require_once __DIR__ . '/preview/index.php';
require_once __DIR__ . '/shortcodes/index.php';
require_once __DIR__ . '/global-data.php';
require_once __DIR__ . '/shared-style.php';
require_once __DIR__ . '/shapes/index.php';
require_once __DIR__ . '/api/index.php';
require_once __DIR__ . '/editor-assets.php';
require_once __DIR__ . '/frontend.php';
require_once __DIR__ . '/add-edit-in-kubio.php';
require_once __DIR__ . '/kubio-block-library.php';
require_once __DIR__ . '/kubio-editor.php';
require_once __DIR__ . '/admin-pages/pages.php';
require_once __DIR__ . '/menu/index.php';
require_once KUBIO_BUILD_DIR . '/third-party-blocks/index.php';

require_once __DIR__ . '/src/DemoSites/WpCliCommand.php';

/** full site editing */
require_once __DIR__ . '/full-site-editing/default-template-types.php';
require_once __DIR__ . '/full-site-editing/block-templates.php';
require_once __DIR__ . '/full-site-editing/templates.php';
require_once __DIR__ . '/full-site-editing/template-parts-area.php';
require_once __DIR__ . '/full-site-editing/class-kubio-rest-template-part-controller.php';
require_once __DIR__ . '/full-site-editing/class-kubio-rest-template-controller.php';
require_once __DIR__ . '/full-site-editing/template-parts.php';
require_once __DIR__ . '/full-site-editing/template-loader.php';
require_once __DIR__ . '/full-site-editing/full-site-editing.php';
require_once __DIR__ . '/full-site-editing/hybrid-theme.php';


function kubio_load_integrations() {
	$integrations_dis = __DIR__ . '/integrations';
	$integrations     = array_diff( scandir( $integrations_dis ), array( '.', '..' ) );

	foreach ( $integrations as $integration ) {
		$integration_entry = "{$integrations_dis}/{$integration}/{$integration}.php";
		if ( file_exists( $integration_entry ) ) {
			require_once $integration_entry;
		}
	}

}

kubio_load_integrations();
Activation::load();
DemoSites::load();
