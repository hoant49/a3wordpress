<?php

function kubio_register_menus_locations() {

	$current_locations = array_keys( get_registered_nav_menus() );

	$kubio_locations = apply_filters(
		'kubio_menus_locations',
		array(
			'header-menu'           => esc_html__( 'Header Menu', 'kubio' ),
			'header-menu-secondary' => esc_html__( 'Secondary Header Menu', 'kubio' ),
			'footer-menu'           => esc_html__( 'Footer Menu', 'kubio' ),
			'footer-menu-secondary' => esc_html__( 'Secondary Footer Menu', 'kubio' ),
		)
	);

	foreach ( $kubio_locations as $location => $title ) {
		if ( in_array( $location, $current_locations ) ) {
			unset( $kubio_locations[ $location ] );
		}
	}

	register_nav_menus( $kubio_locations );
}

add_action( 'after_setup_theme', 'kubio_register_menus_locations', 100 );
