<?php

namespace Kubio\DemoSites;

use IlluminateAgnostic\Arr\Support\Arr;

class DemoSitesRepository {

	const DEMO_SITES_TRANSIENT = 'kubio-demo-sites-repository';
	private static $instance   = null;
	public $demos              = array();

	public function __construct() {

		$data = get_transient( self::DEMO_SITES_TRANSIENT );
		if ( $data !== false ) {
			$this->demos = $data;
		} else {
			$this->prepareRetrieveDemoSites();
		}

		add_filter( 'kubio/demo-sites/list', array( $this, 'getDemoSitesList' ) );
		add_action( 'wp_ajax_kubio-demo-sites-retrieve', array( $this, 'actionRetrieveDemoSites' ) );
	}

	public function actionRetrieveDemoSites() {
		return $this->retrieveDemoSites();
	}

	public function prepareRetrieveDemoSites() {

		add_action(
			'admin_footer',
			function () {
				$fetch_url = add_query_arg(
					array( 'action' => 'kubio-demo-sites-retrieve' ),
					admin_url( 'admin-ajax.php' )
				);
				?>
			<script>
				window.fetch("<?php echo esc_js( $fetch_url ); ?>")
			</script>
				<?php
			}
		);
	}

	public static function getDemos( $with_internals = false ) {
		$demos = static::getInstance()->demos;

		if ( ! $with_internals ) {
			foreach ( $demos as $slug => $demo ) {
				if ( Arr::get( $demo, 'internal', false ) ) {
					unset( $demos[ $slug ] );
				}
			}
		}

		return  $demos;
	}

	/**
	 * @return DemoSitesRepository
	 */
	private static function getInstance() {
		static::load();

		return static::$instance;
	}

	public static function load() {
		if ( ! static::$instance ) {
			static::$instance = new DemoSitesRepository();
		}
	}

	public function getDemoSitesList() {
		return $this->demos;
	}

	public function retrieveDemoSites( $print = true, $verifySSL = true ) {
		$demos = array();

		foreach ( $this->getLocalDemoSites() as $site ) {
			$slug = Arr::get( $site, 'slug', null );

			if ( $slug ) {
				$demos[ $slug ] = $site;
			}
		}

		foreach ( $this->getRemoteDemoSites( $verifySSL ) as $site ) {
			$slug = Arr::get( $site, 'slug', null );

			if ( $slug ) {
				$demos[ $slug ] = $site;
			}
		}

		// remove keys from assoc array
		$this->demos = $demos;

		set_transient( self::DEMO_SITES_TRANSIENT, $this->demos, DAY_IN_SECONDS );

		if ( $print ) {
			wp_send_json_success( $this->demos );
		}
	}

	private function getLocalDemoSites() {
		return apply_filters( 'kudio/demo-sites/local-demos', array() );
	}

	public function getRemoteDemoSites( $verifySSL = true ) {
		$url = apply_filters( 'kubio/demo-sites/demos-url', 'https://demos.kubiobuilder.com/' );

		if ( ! $url ) {
			return array();
		}

		$response = wp_remote_get(
			$url,
			array(
				'sslverify' => $verifySSL,
			)
		);

		if ( $response instanceof \WP_Error ) {
			return array();
		}

		if ( wp_remote_retrieve_response_code( $response ) !== 200 ) {
			return array();
		}

		$body = wp_remote_retrieve_body( $response );

		$demos = json_decode( $body, true );

		if ( json_last_error() !== JSON_ERROR_NONE ) {
			return array();
		}

		return $demos;
	}

}
