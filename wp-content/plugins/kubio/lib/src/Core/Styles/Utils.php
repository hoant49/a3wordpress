<?php


namespace Kubio\Core\Styles;

use Kubio\Core\LodashBasic;
use function implode;

use Kubio\Config;

class Utils {

	public static function composeClassesByMedia( $valuesByMedia, $prefix, $allow_empty = false ) {
		$classes = array();
		foreach ( $valuesByMedia as $media => $value ) {
			if ( $value !== null || $allow_empty ) {
				$classes[] = self::composeClassForMedia( $media, $value, $prefix, $allow_empty );
			}
		}

		return $classes;
	}

	public static function composeClassForMedia( $media, $value, $prefix, $allow_empty = false ) {
		if ( ! $allow_empty ) {
			$isEmptyString = is_string( $value ) && strlen( $value ) === 0;
			if ( $isEmptyString ) {
				return '';
			}
		}
		$mediaPrefix   = self::getMediaPrefix( $media );
		$values        = LodashBasic::compactWithExceptions( array( $prefix, $mediaPrefix, $value ), array( '0', 0 ) );
		$prefixedClass = implode( '-', $values );

		return $prefixedClass;
	}

	public static function getMediaPrefix( $media ) {
		return LodashBasic::get( Config::mediasById(), $media . '.' . 'gridPrefix', false );
	}

}

