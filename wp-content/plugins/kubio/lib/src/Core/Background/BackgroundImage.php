<?php

namespace Kubio\Core\Background;

use Kubio\Config;
use Kubio\Core\Element;
use Kubio\Core\ElementBase;
use Kubio\Core\Utils;

class BackgroundImage extends ElementBase {
	function __construct( $value ) {
		parent::__construct( $value, BackgroundDefaults::getDefaultImage() );
	}


	function wrapperComputedStyle() {
		$style = array();

		if ( $this->useParallaxScript() ) {
			if ( $this->useFeaturedImage() ) {
				$url = get_the_post_thumbnail_url( null, 'full' );
			} else {
				$url = $this->get( '0.source.url' );
			}
			$style['backgroundImage'] = "url(\"$url\")";
		} else {
			// todo implement the style for image layer when it will be needed with not parallax.
			// It will be needed for  ken burner effect in slider
			$image = $this->get( '0' );
		}
		return $style;
	}

	function useParallaxScript() {
		return $this->get( '0.useParallax' );
	}
	function useFeaturedImage() {
		return $this->get( '0.useFeaturedImage' );
	}
	function getClasses() {
		$classes = array( 'background-layer' );
		if ( $this->useParallaxScript() ) {
			$classes[] = 'paraxify';
		}

		return $classes;
	}

	function __toString() {
		$classes = $this->getClasses();

		$scriptData = Utils::useJSComponentProps(
			'parallax',
			array(
				'enabled' => $this->useParallaxScript(),
				'test'    => 'temp',
			)
		);

		return new Element(
			Element::DIV,
			array_merge(
				$scriptData,
				array(
					'style'     => $this->wrapperComputedStyle(),
					'className' => $classes,
				)
			)
		) . '';

	}
}
