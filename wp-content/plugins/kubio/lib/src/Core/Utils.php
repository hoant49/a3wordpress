<?php


namespace Kubio\Core;


use Kubio\Config;
use function json_encode;

class Utils {
	public static function mapHideClassesByMedia(
		$hiddenByMedia,
		$negateValue = false
	) {
		$mapHideClassesByMedia = array();
		foreach ( $hiddenByMedia as $media => $isHidden ) {
			if ( $negateValue ) {
				$isHidden = ! $isHidden;
			}
			if ( $isHidden ) {
				array_push( $mapHideClassesByMedia, "kubio-hide-on-$media" );
			}
		}

		return $mapHideClassesByMedia;
	}

	public static function useJSComponentProps( $name, $settings = array() ) {
		$prefix = Config::$name;

		return array(
			"data-${prefix}-component" => $name,
			"data-${prefix}-settings"  => json_encode( $settings ),
		);
	}

	public static function getLinkAttributes( $linkObject ) {
		$defaultValue     = array(
			'value'         => '',
			'typeOpenLink'  => 'sameWindow',
			'noFollow'      => false,
			'lightboxMedia' => '',
		);
		$mergedLinkObject = LodashBasic::merge( array(), $defaultValue, $linkObject );
		$linkAttributes   = array(
			'href'                 => null,
			'target'               => null,
			'rel'                  => null,
			'data-kubio-component' => null,
		);

		if ( $mergedLinkObject ) {
			if ( $mergedLinkObject['value'] ) {
				$linkAttributes['href'] = $mergedLinkObject['value'];
			}
			$linkType = LodashBasic::get( $mergedLinkObject, 'typeOpenLink', '' );
			if ( $linkType === 'newWindow' ) {
				$linkAttributes['target'] = '_blank';
			}

			if ( $linkType === 'lightbox' ) {
				$lightboxType = $mergedLinkObject['lightboxMedia'];
				if ( $lightboxType === '' ) {
					$lightboxType = null;
				}
				$linkAttributes['data-default-type'] = $lightboxType;
				$linkAttributes['data-fancybox']     = rand() . '';
			}
			if ( $mergedLinkObject['noFollow'] ) {
				$linkAttributes['rel'] = 'nofollow';
			}
		}

		return $linkAttributes;
	}

	public static function shortcodeDecode( $data ) {
		return urldecode( base64_decode( $data ) );
	}

	public static function getDefaultAssetsUrl( $url ) {
		$staticUrl = kubio_url( 'static/default-assets' );

		return $staticUrl . '/' . ltrim( $url, '/' );
	}

	public static function canEdit() {
		return current_user_can( 'edit_theme_options' ) && current_user_can( 'edit_posts' );
	}

	public static function getEmptyShortcodePlaceholder() {
		if ( is_user_logged_in() ) {
			return static::getFrontendPlaceHolder(
				sprintf(
					'%s<br/><div class="kubio-frontent-placeholder--small">%s</div>',
					__( 'Shortcode is empty.', 'kubio' ),
					__( 'Edit this page to insert a shortcode or delete this block.', 'kubio' )
				)
			);
		} else {
			return '';
		}

	}

	public static function getFrontendPlaceHolder( $message, $options = array() ) {

		$options = array_merge(
			array(
				'info'  => true,
				'title' => __( 'Kubio info', 'kubio' ),
			),
			$options
		);

		if ( is_callable( $message ) ) {
			$message = call_user_func( $message );
		}

		$info = '';
		if ( $options['info'] ) {
			$info = sprintf(
				'<div class="kubio-frontent-placeholder--info">' .
				'	<div class="kubio-frontent-placeholder--logo">%s</div>' .
				'   <div class="kubio-frontent-placeholder--title">%s</div>' .
				'</div>',
				wp_kses_post( KUBIO_LOGO_SVG ),
				$options['title']
			);
		}

		return sprintf( '<div class="kubio-frontent-placeholder"><div>%s</div><div>%s</div></div>', $info, $message );
	}

	public static function kubioCacheGet( $name, $default = null ) {

		$kubio_cache = isset( $GLOBALS['__kubio_plugin_cache__'] ) ? $GLOBALS['__kubio_plugin_cache__'] : array();
		$value       = $default;

		if ( self::kubioCacheHas( $name ) ) {
			$value = $kubio_cache[ $name ];
		}

		return $value;

	}

	public static function kubioCacheHas( $name ) {
		$kubio_cache = isset( $GLOBALS['__kubio_plugin_cache__'] ) ? $GLOBALS['__kubio_plugin_cache__'] : array();

		return array_key_exists( $name, $kubio_cache );
	}

	public static function kubioCacheSet( $name, $value ) {
		$kubio_cache          = isset( $GLOBALS['__kubio_plugin_cache__'] ) ? $GLOBALS['__kubio_plugin_cache__'] : array();
		$kubio_cache[ $name ] = $value;

		$GLOBALS['__kubio_plugin_cache__'] = $kubio_cache;

	}

	/**
	 * Remove empty branches from array
	 *
	 * @param array $array the array to walk
	 *
	 * @return array
	 */
	public static function arrayRecursiveRemoveEmptyBranches( array &$array ) {
		foreach ( $array as $key => &$value ) {
			if ( is_array( $value ) ) {
				$array[ $key ] = static::arrayRecursiveRemoveEmptyBranches( $value );

				if ( empty( $value ) ) {
					unset( $array[ $key ] );
				}
			}
		}

		return $array;
	}

	public static function walkBlocks( &$blocks, $callback ) {
		array_walk(
			$blocks,
			function ( &$block ) use ( $callback ) {
				if ( isset( $block['blockName'] ) ) {
					$callback( $block );
				}
				if ( isset( $block['innerBlocks'] ) ) {
					static::walkBlocks( $block['innerBlocks'], $callback );
				}
			}
		);
	}

	public static function kses( $text, $allowed_protocols = array() ) {

		static $allowed_html;

		if ( ! $allowed_html ) {
			$allowed_html = wp_kses_allowed_html( 'post' );
		}

		// fix the issue with rgb / rgba colors in style atts

		$rgbRegex = '#rgb\(((?:\s*\d+\s*,){2}\s*[\d]+)\)#i';
		$text     = preg_replace( $rgbRegex, 'rgb__$1__rgb', $text );

		$rgbaRegex = '#rgba\(((\s*\d+\s*,){3}[\d\.]+)\)#i';
		$text      = preg_replace( $rgbaRegex, 'rgba__$1__rgb', $text );

		$text = wp_kses( $text, $allowed_html, $allowed_protocols );

		$text = str_replace( 'rgba__', 'rgba(', $text );
		$text = str_replace( 'rgb__', 'rgb(', $text );
		$text = str_replace( '__rgb', ')', $text );

		return $text;
	}

	public static function ksesSVG( $svg_content ) {
		$allowed_html = wp_kses_allowed_html( 'post' );
		return wp_kses( $svg_content, $allowed_html );
	}
}
