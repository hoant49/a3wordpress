<?php

namespace Kubio\Core\StyleManager;

use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\Config;
use Kubio\Core\Blocks\BlockBase;
use Kubio\Core\Blocks\DataHelper;
use Kubio\Core\LodashBasic;

class GlobalStyleRender extends StyleRender {

	const V_SPACE_NEGATIVE = 'vSpaceNegative';
	const V_SPACE          = 'vSpace';
	const H_SPACE_GROUP    = 'hSpaceGroup';
	const H_SPACE          = 'hSpace';
	const GLOBAL_PREFIX    = '#kubio';

	private $dataHelper = null;
	public function __construct( $main_attr ) {
		$main_attr         = $this->removeDisabledRules( $main_attr );
		 $this->dataHelper = new DataHelper( array(), array( Config::$mainAttributeKey => $main_attr ) );

		$styledElementsByName = Config::value( 'definitions.globalStyle.elementsByName' );
		$styledElementsEnum   = Config::value( 'definitions.globalStyle.elementsEnum' );

		$styledElementsByName = $this->maybePrefixStyledElements( $styledElementsByName );

		$normalized  = self::normalizeData( $main_attr, $styledElementsByName, $styledElementsEnum );
		$this->model = (object) LodashBasic::get( $normalized, 'model', array() );
		parent::__construct(
			array(
				'styledElementsByName' => $styledElementsByName,
				'styledElementsEnum'   => $styledElementsEnum,
				'wrapperElement'       => false,
			)
		);
	}


	public function maybePrefixStyledElements( $styledElementsByName ) {
		foreach ( $styledElementsByName as $name => $styled_element ) {

			if ( isset( $styled_element['selector'] ) && Arr::get( $styled_element, 'withGlobalPrefix', false ) ) {

				$selector = $styled_element['selector'];

				if ( is_string( $selector ) ) {
						$styled_element['selector'] = $this->prefixSelector( $selector, GlobalStyleRender::GLOBAL_PREFIX );
				} else {
					foreach ( $selector as $state => $state_selector ) {
						$styled_element['selector'][ $state ] = $this->prefixSelector( $state_selector, GlobalStyleRender::GLOBAL_PREFIX );
					}
				}

				$styledElementsByName[ $name ] = $styled_element;
			}
		}

		return $styledElementsByName;
	}

	private function prefixSelector( $selector, $prefix ) {
		$selector_parts = explode( ',', $selector );

		foreach ( $selector_parts as $index => $value ) {
			$selector_parts[ $index ] = $prefix . ' ' . trim( $value );
		}

		return implode( ',', $selector_parts );
	}

	public function removeDisabledRules( $main_attr ) {
		$disabled       = LodashBasic::get( $main_attr, 'style.disabled', array() );
		$disabled_rules = array_filter(
			array_keys( $disabled ),
			function( $item ) use ( $disabled ) {
				return  LodashBasic::get( $disabled, $item, false );
			}
		);

		$disabled_rules = array_reduce(
			$disabled_rules,
			function( $result, $item ) {
				if ( $item === 'headings' ) {
					$result = array_merge(
						$result,
						array(
							'h1',
							'h2',
							'h3',
							'h4',
							'h4',
							'h5',
							'h6',
						)
					);
				} else {
					array_push( $result, $item );
				}

				return $result;
			},
			array()
		);

		if ( count( $disabled_rules ) ) {
			$body_style = LodashBasic::get( $main_attr, 'style.descendants.body', array() );
			LodashBasic::each(
				$disabled_rules,
				function( $rule ) use ( $body_style ) {
					LodashBasic::set( $body_style, "typography.holders.{$rule}", array() );
				}
			);

			$medias = LodashBasic::get( $body_style, 'media', array() );

			LodashBasic::each(
				$medias,
				function( $media ) use ( $body_style ) {
					LodashBasic::each(
						$media,
						function( $rule ) use ( $body_style, $media ) {
							LodashBasic::set( $body_style, "{$media}.typography.holders.{$rule}", array() );
						}
					);
				}
			);

			$main_attr = LodashBasic::set( $main_attr, 'style.descendants.body', $body_style );
		}

		return $main_attr;
	}

	public function getDynamicStyle() {
		 $vSpaceByMedia = $this->dataHelper->getPropByMedia( 'vSpace' );
		$hSpaceByMedia  = $this->dataHelper->getPropByMedia( 'hSpace' );

		return  self::normalizeDynamicStyle(
			array(
				self::V_SPACE_NEGATIVE => DynamicStyles::vSpace(
					$vSpaceByMedia,
					true
				),
				self::V_SPACE          => DynamicStyles::vSpace(
					$vSpaceByMedia
				),
				self::H_SPACE_GROUP    => DynamicStyles::hSpaceParent(
					$hSpaceByMedia
				),
				self::H_SPACE          => DynamicStyles::hSpace(
					$hSpaceByMedia
				),
			)
		);
	}

	public function export( $dynamicStyle = null ) {
		$style         = $this->model->style['shared'];
		$css['global'] = $this->convertStyleToCss(
			$style,
			array(
				'styledElementsByName' => $this->styledElementsByName,
				'styleType'            => 'global',
			)
		);

		$css['dynamic'] = $this->convertStyleToCss(
			$this->getDynamicStyle(),
			array(
				'styledElementsByName' => $this->styledElementsByName,
				'styleType'            => 'global',
			)
		);

		return $css;
	}
}
