<?php


namespace Kubio\Core\StyleManager\Props;

use Kubio\Config;
use Kubio\Core\LodashBasic;
use Kubio\Core\Registry;
use Kubio\Core\StyleManager\BlockStyleRender;
use Kubio\Core\StyleManager\ParserUtils;
use Kubio\Core\StyleManager\StyleParser;
use function join;

class Typography extends PropertyBase {

	public function getSelector( $name ) {
		switch ( $name ) {
			case 'lead':
				return '& .h-lead p, & .h-lead';

			case 'blockquote':
				return '& ' . join( ',', array( 'blockquote p' ) );

			default:
				return '& ' . $name . '';
		}
	}

	public function getTypographyCss( $value ) {
		$style              = array();
		$styleParserClass   = StyleParser::getInstance();
		$propertiesMap      = Config::value( 'props.typography.config.map' );
		$unitLessProperties = array( 'lineHeight' );

		// Fonts
		Registry::getInstance()->registerFonts( LodashBasic::get( $value, 'family' ), LodashBasic::get( $value, 'weight' ) );
		// add fallback fonts - increase performance scores
		$value['family'] = LodashBasic::get( $value, 'family' ) ? LodashBasic::get( $value, 'family' ) . ',Helvetica, Arial, Sans-Serif, serif' : '';

		// Remaining properties
		$style = LodashBasic::merge( $style, ParserUtils::addPrimitiveValues( $style, $value, $propertiesMap, $unitLessProperties ) );

		return $style;
	}

	public function parseHolders( $typography ) {
		$statesById = Config::statesById();
		$holders    = LodashBasic::omit( $typography, array( 'states' ) );
		$collected  = array();
		foreach ( $holders as $name => $___ ) {
			$holderTypography = $holders[ $name ];
			$normal           = LodashBasic::omit( $holderTypography, array( 'states' ) );
			$byState          = LodashBasic::merge( array( 'normal' => $normal ), LodashBasic::get( $holderTypography, 'states', array() ) );

			foreach ( $byState as $stateName => $___ ) {
				$stateTypography = $byState[ $stateName ];
				$val             = $this->valueWithDefault( $stateTypography );
				$selector        = LodashBasic::get( $statesById, array( $stateName, 'selector' ), '' );

				LodashBasic::set(
					$collected,
					array(
						$this->getSelector( $name ),
						',&[data-kubio]',
						'&' . $selector,
					),
					$this->getTypographyCss( $val )
				);

				LodashBasic::unsetValue( $typography, $name );
			}
		}
		return $collected;
	}

	public function parse( $value, $options ) {
		$holders     = LodashBasic::get( $value, 'holders', array() );
		$holdersTypo = $this->parseHolders( $holders );
		$textDefault = array();
		if ( LodashBasic::has( $holders, 'p' ) ) {
			$textDefault = $holders['p'];
			if ( LodashBasic::has( $textDefault, 'margin' ) ) {
				unset( $textDefault->margin );
			}
		}
		$nodeTypography = LodashBasic::merge( array(), $this->config( 'default' ), $textDefault, LodashBasic::omit( $value, 'holders' ) );
		$nodeTypo       = $this->getTypographyCss( $nodeTypography );
		return LodashBasic::merge( $nodeTypo, $holdersTypo );
	}
}
