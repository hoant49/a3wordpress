<?php

namespace Kubio\Core;

use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\Flags;
use WP_Error;

class Activation {

	private static $instance = null;

	private $remote_content = array();

	public function __construct() {
		add_action(
			'activated_plugin',
			function ( $plugin ) {
				if ( $plugin == plugin_basename( KUBIO_ENTRY_FILE ) ) {
					$hash = uniqid( 'activate-' );
					Flags::set( 'activation-hash', $hash );
					$url = add_query_arg(
						array(
							'page'                  => 'kubio-get-started',
							'kubio-activation-hash' => $hash,
						),
						admin_url( 'admin.php' )
					);
					if ( ! $this->isCLI() ) {
						wp_redirect( $url );
						exit();
					} else {
						Flags::set( 'import_design', false );
					}
				}
			}
		);

		$self = $this;
		add_action(
			'init',
			function () use ( $self ) {
				$hash       = sanitize_text_field( Arr::get( $_REQUEST, 'kubio-activation-hash', null ) );
				$saved_hash = Flags::get( 'activation-hash', false );
				if ( $saved_hash === $hash ) {
					Flags::delete( 'activation-hash' );
					$self->activate();
				}
			},
			500
		);

		add_action( 'after_switch_theme', array( $this, 'afterSwitchTheme' ) );

	}

	public function isCLI() {
		return defined( 'WP_CLI' ) && WP_CLI;
	}

	public function activate() {

		if ( ! Flags::get( 'kubio_activation_time', false ) ) {
			Flags::set( 'kubio_activation_time', time() );
		}

		if ( ! kubio_theme_has_block_templates_support() ) {
			return;
		}

		add_filter(
			'kubio/importer/kubio-url-placeholder-replacement',
			array( $this, 'getUrlPlaceholderReplacement' )
		);

		$this->addCommonFilters();
		add_filter( 'kubio/importer/available_templates', array( $this, 'getAvailableTemplates' ), 10 );
		add_filter( 'kubio/importer/available_template_parts', array( $this, 'getAvailableTemplateParts' ), 10 );

		$with_front_page = Flags::get( 'import_design', false ) !== false;

		$this->prepareRemoteData( $with_front_page );

		if ( $with_front_page ) {
			add_filter( 'kubio/importer/page_path', array( $this, 'getDesignPagePath' ), 10, 2 );
			$this->importDesign();
		} else {
			add_filter( 'kubio/importer/content', array( $this, 'importCustomizerOptions' ), 20, 3 );
			$this->createPages( array( 'with_blog_page' => true ) );
		}

		$this->importTemplates();
		$this->importTemplateParts();

	}

	public function addCommonFilters() {
		add_filter( 'kubio/importer/skip-remote-file-import', '__return_true' );
		add_filter( 'kubio/importer/content', array( $this, 'getFileContent' ), 1, 3 );
		add_filter( 'kubio/importer/content', array( $this, 'templateMapPartsTheme' ), 10, 2 );
		add_filter( 'kubio/importer/content', array( $this, 'importAssets' ), 10, 1 );
		remove_filter( 'theme_mod_nav_menu_locations', 'kubio_nav_menu_locations_from_global_data' );
		remove_filter( 'wp_insert_post_data', 'kubio_on_post_update', 10, 3 );
		remove_action( 'wp_insert_post', 'kubio_update_meta', 10, 3 );
	}

	public function prepareRemoteData( $with_front_page = false ) {
		$base_url  = 'https://themes.kubiobuilder.com';
		$file_name = get_stylesheet() . '__' . get_template() . '__' . ( $with_front_page ? 'with-front' : 'default' ) . '.data';
		$response  = wp_safe_remote_get( "{$base_url}/{$file_name }" );

		if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ) {
			$content = wp_remote_retrieve_body( $response );
			$data    = unserialize( $content );

			if ( ! is_array( $data ) || Arr::get( $data, 'error' ) ) {
				return;
			}

			$this->remote_content = $data;

			if ( $global_data = Arr::get( $this->remote_content, 'global-data' ) ) {
				$global_data = json_decode( $global_data, true );

				if ( json_last_error() === JSON_ERROR_NONE ) {
					// remove the menuLocations key in global data. We don't need that
					Arr::forget( $global_data, 'menuLocations' );
					kubio_replace_global_data_content( $global_data );
				}
			}
		}

	}

	public function importDesign() {
		$result = $this->setPages();

		// try to set the blog page and menu
		if ( ! is_wp_error( $result ) ) {
			$this->preparePrimaryMenu();
		}

	}

	private function setPages( $data = array() ) {

		$data = array_merge(
			array(
				'front_content'  => null,
				'with_blog_page' => true,
			),
			$data
		);

		$front_page_id = $this->importFrontPage( $data['front_content'] );

		if ( is_wp_error( $front_page_id ) ) {
			return $front_page_id;
		}

		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $front_page_id );

		$posts_page_id = intval( get_option( 'page_for_posts' ) );

		if ( ! $posts_page_id && $data['with_blog_page'] ) {
			$posts_page_id = wp_insert_post(
				array(
					'comment_status' => 'closed',
					'ping_status'    => 'closed',
					'post_name'      => 'blog',
					'post_title'     => __( 'Blog', 'kubio' ),
					'post_status'    => 'publish',
					'post_type'      => 'page',
					'page_template'  => apply_filters(
						'kubio/front_page_template',
						'page-templates/homepage.php'
					),
					'post_content'   => '',
				)
			);

			if ( ! is_wp_error( $posts_page_id ) ) {
				update_option( 'page_for_posts', $posts_page_id );
			}
		}

		return $posts_page_id;
	}

	/**
	 * @param mixed|null $content
	 *
	 * @return int|WP_Error
	 */
	private function importFrontPage( $content = null ) {

		$page_on_front = get_option( 'page_on_front' );
		$query         = new \WP_Query(
			array(
				'post__in'    => array( $page_on_front ),
				'post_status' => array( 'publish' ),
				'fields'      => 'ids',
				'post_type'   => 'page',
			)
		);

		if ( $query->have_posts() ) {
			return intval( $page_on_front );
		}

		if ( $content === null ) {
			$content = Importer::getTemplateContent( 'page', 'front-page' );

			if ( ! $content ) {
				return new WP_Error();
			}
		}

		return wp_insert_post(
			array(
				'comment_status' => 'closed',
				'ping_status'    => 'closed',
				'post_name'      => 'front_page',
				'post_title'     => __( 'Front Page', 'kubio' ),
				'post_status'    => 'publish',
				'post_type'      => 'page',
				'page_template'  => apply_filters(
					'kubio/front_page_template',
					'page-templates/homepage.php'
				),
				'post_content'   => wp_slash( kubio_serialize_blocks( parse_blocks( $content ) ) ),
			)
		);

	}

	private function preparePrimaryMenu() {
		$theme_menu_locations    = array_keys( get_registered_nav_menus() );
		$common_header_locations = array(
			'header-menu',
			'header',
			'primary',
			'main',
			'menu-1',
		);

		$selected_location = null;
		/**
		 *  Try to make an educated guess and primary menu location
		 */
		foreach ( $theme_menu_locations as $location ) {
			foreach ( $common_header_locations as $common_header_location ) {
				if ( stripos( $location, $common_header_location ) !== false ) {
					$selected_location = $location;
					break;
				}
			}

			if ( $selected_location ) {
				break;
			}
		}

		$selected_location = apply_filters( 'kubio/primary_menu_location', $selected_location );

		if ( $selected_location ) {

			$current_set_locations = get_nav_menu_locations();

			$primary_menu_id = Arr::get( $current_set_locations, $selected_location, null );

			if ( ! $primary_menu_id ) {
				$primary_menu_id = wp_create_nav_menu( __( 'Primary menu', 'kubio' ) );
			}

			if ( is_wp_error( $primary_menu_id ) ) {
				return;
			}

			$menu_items     = wp_get_nav_menu_items( $primary_menu_id );
			$has_front_page = false;
			$has_blog_page  = false;

			foreach ( $menu_items as $menu_item ) {

				if ( ! $has_front_page ) {
					$menu_item_object_is_front_page = $menu_item->type === 'post_type' && $menu_item->object === 'page' && intval( $menu_item->object_id ) === intval( get_option( 'page_on_front' ) );
					$custom_url                     = $menu_item->type === 'custom' ? $menu_item->url : null;
					$menu_item_link_is_front_page   = false;
					$parsed_url                     = parse_url( $custom_url );

					if ( $parsed_url && $custom_url ) {
						$site_url = site_url();

						$parsed_url = array_merge(
							array(
								'scheme' => '',
								'host'   => '',
								'path'   => '',
							),
							$parsed_url
						);

						$menu_item_url                = "{$parsed_url['scheme']}://{$parsed_url['host']}{$parsed_url['path']}";
						$menu_item_link_is_front_page = untrailingslashit( $menu_item_url ) === untrailingslashit( $site_url );
					}

					if ( $menu_item_object_is_front_page || $menu_item_link_is_front_page ) {
						$has_front_page = true;
					}
				}

				if ( $menu_item->type === 'post_type' && $menu_item->object === 'page' && intval( $menu_item->object_id ) === intval( get_option( 'page_for_posts' ) ) ) {
					$has_blog_page = true;
				}
			}

			if ( ! $has_front_page ) {
				wp_update_nav_menu_item(
					$primary_menu_id,
					0,
					array(
						'menu-item-title'     => __( 'Front Page', 'kubio' ),
						'menu-item-object'    => 'page',
						'menu-item-object-id' => get_option( 'page_on_front' ),
						'menu-item-type'      => 'post_type',
						'menu-item-status'    => 'publish',
					)
				);
			}

			if ( ! $has_blog_page ) {
				wp_update_nav_menu_item(
					$primary_menu_id,
					0,
					array(
						'menu-item-title'     => __( 'Blog', 'kubio' ),
						'menu-item-object'    => 'page',
						'menu-item-object-id' => get_option( 'page_for_posts' ),
						'menu-item-type'      => 'post_type',
						'menu-item-status'    => 'publish',
					)
				);
			}

			set_theme_mod(
				'nav_menu_locations',
				array_merge(
					$current_set_locations,
					array(
						$selected_location => $primary_menu_id,
					)
				)
			);
		}
	}

	public function createPages( $options = array() ) {
		$result = $this->setPages(
			array_merge(
				array(
					'front_content'  => '',
					'with_blog_page' => false,
				),
				$options
			)
		);

		// try to set the blog page and menu
		if ( ! is_wp_error( $result ) ) {
			$this->preparePrimaryMenu();
		}

	}

	private function importTemplates() {
		$entities = array_keys( Importer::getAvailableTemplates() );

		foreach ( $entities as $slug ) {
			Importer::createTemplate( $slug, Importer::getTemplateContent( 'wp_template', $slug ) );
		}
	}

	private function importTemplateParts() {
		$entities = array_keys( Importer::getAvailableTemplateParts() );

		foreach ( $entities as $slug ) {

			Importer::createTemplatePart( $slug, Importer::getTemplateContent( 'wp_template_part', $slug ) );
		}
	}

	public static function load() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function afterSwitchTheme() {
		$this->addCommonFilters();

		$this->createPages();
		$this->importTemplates();
		$this->importTemplateParts();
	}

	public function getUrlPlaceholderReplacement() {
		$stylesheet = get_stylesheet();

		return "https://static-assets.kubiobuilder.com/themes/{$stylesheet}/assets/";
	}

	public function getAvailableTemplates() {
		$templates = Arr::get( $this->remote_content, 'block-templates', array() );

		foreach ( array_keys( $templates ) as $template ) {
			$templates[ $template ] = null;
		}

		return $templates;
	}

	public function getAvailableTemplateParts() {
		$templates = Arr::get( $this->remote_content, 'block-template-parts', array() );

		foreach ( array_keys( $templates ) as $template ) {
			$templates[ $template ] = null;
		}

		return $templates;
	}

	public function getDesignPagePath( $path, $slug ) {
		if ( $slug === 'front-page' ) {
			return null;

		}

		return $path;
	}

	public function importAssets( $content ) {
		$blocks = parse_blocks( $content );
		$blocks = Importer::maybeImportBlockAssets( $blocks );

		return kubio_serialize_blocks( $blocks );
	}

	public function getFileContent( $content, $type, $slug ) {

		if ( $content !== null ) {
			return $content;
		}

		$category = '';
		switch ( $type ) {
			case 'wp_template':
				$category = 'block-templates';
				break;
			case 'wp_template_part':
				$category = 'block-template-parts';
				break;
			case 'page':
				$category = 'pages';
				break;
		}

		return Arr::get( $this->remote_content, "{$category}.{$slug}", '' );
	}

	public function templateMapPartsTheme( $content, $type ) {

		if ( $type === 'wp_template' ) {
			$blocks = parse_blocks( $content );
			// $parts_block_names = array( 'kubio/header', 'kubio/footer', 'kubio/sidebar', 'core/template-part' );
			$updated_blocks = kubio_blocks_update_template_parts_theme( $blocks, get_stylesheet() );

			return kubio_serialize_blocks( $updated_blocks );
		}

		return $content;
	}

	public function importCustomizerOptions( $content, $type, $slug ) {
		$customizer_importer = new CustomizerImporter( $content, $type, $slug );

		return $customizer_importer->process();
	}


}
