<?php

use IlluminateAgnostic\Arr\Support\Arr;
use Kubio\Core\Importer;
use Kubio\Core\LodashBasic;
use Kubio\Core\Utils;

add_filter(
	'kubio/preview/template_part_blocks',
	function ( $parts = array() ) {
		return array_merge(
			(array) $parts,
			array(
				'core/template-part',
				'kubio/header',
				'kubio/footer',
				'kubio/sidebar',
			)
		);
	},
	5,
	1
);


function kubio_blocks_update_template_parts_theme( $parsed_blocks, $theme ) {
	$parts_block_names = apply_filters( 'kubio/preview/template_part_blocks', array() );

	Utils::walkBlocks(
		$parsed_blocks,
		function ( &$block ) use ( $theme, $parts_block_names ) {

			$block_name       = Arr::get( $block, 'blockName' );
			$current_theme    = Arr::get( $block, 'attrs.theme' );
			$is_template_part = in_array( $block_name, $parts_block_names );

			if ( $block_name && ( $current_theme || $is_template_part ) ) {
				Arr::set( $block, 'attrs.theme', $theme );
			}

		}
	);

	return $parsed_blocks;
}


function kubio_rest_pre_insert_import_assets( $prepared_post ) {

	//if you make changes to the post that does not include it's content. For example featured image, template,
	// slug etc... . we need to stop the function or the post content will be removed
	if ( ! isset( $prepared_post->post_content ) ) {
		return $prepared_post;
	}
	$content = $prepared_post->post_content;

	$blocks = parse_blocks( $content );

	$blocks                      = Importer::maybeImportBlockAssets( $blocks );
	$prepared_post->post_content = kubio_serialize_blocks( $blocks );

	return $prepared_post;
}

function kubio_import_assets_filter() {
	$post_types = array( 'page', 'post', 'wp_template', 'wp_template_part' );

	foreach ( $post_types as $post_type ) {
		add_filter( "rest_pre_insert_{$post_type}", 'kubio_rest_pre_insert_import_assets' );
	}
}

add_action( 'init', 'kubio_import_assets_filter' );

//this code is not required for the attributes to work. But it could be a problem in the future if we don't register the
//anchor attribute
function kubio_register_anchor_attribute( $metaData ) {
	$supportsAnchor = LodashBasic::get( $metaData, array( 'supports', 'anchor' ), false );
	if ( $supportsAnchor ) {
		$hasAnchorAttribute = LodashBasic::get( $metaData, array( 'attributes', 'anchor' ), false );
		if ( ! $hasAnchorAttribute ) {
			$anchorData = array(
				'type' => 'string',
			);
			LodashBasic::set( $metaData, array( 'attributes', 'anchor' ), $anchorData );
		}
	}

	return $metaData;
}

add_filter(
	'kubio/blocks/register_block_type',
	'kubio_register_anchor_attribute'
);


function kubio_add_full_hd_image_size() {
	add_image_size( 'kubio-fullhd', 1920, 1080 );
}

add_filter( 'after_setup_theme', 'kubio_add_full_hd_image_size' );


function kubio_url_import_cdn_files( $url ) {

	if ( strpos( $url, 'wp-content/uploads' ) !== false ) {

		if ( \_\startsWith( $url, site_url() ) ) {
			return $url;
		}

		return str_replace( 'https://demos.kubiobuilder.com', 'https://static-assets.kubiobuilder.com/demos', $url );
	}

	return $url;
}

add_filter( 'kubio/importer/kubio-source-url', 'kubio_url_import_cdn_files' );


//load full width template if the page is empty
add_action(
	'wp',
	function () {
		/** @var WP_Query $wp_query */

		//only apply to pages
		if ( is_page() && ! is_front_page() ) {
			global $post;
			$template      = get_page_template_slug( $post->ID );
			$referer       = Arr::get( $_SERVER, 'HTTP_REFERER', '' );
			$callFromKubio = strpos( $referer, 'page=kubio' ) !== false && strpos( $referer, admin_url() ) !== false;
			if ( empty( trim( $post->post_content ) ) && isset( $_GET['_wp-find-template'] ) && $callFromKubio && empty( $template ) ) {
				$templates = array( 'full-width.php' );

				/**
				 * The locate_block_template function has a check if the get parameter the _wp-find-template is set it
				 * returns wp_send_json_success( $block_template ) with the template provided;
				 * the  wp_send_json_success( $block_template );
				 *
				 * If the full width template is found the wp_send_json_success will return the full width tempalte then die
				 * the request. If the full width template is not found then the function will return the 'full-width' text
				 * and we do nothing with it. But the code will run normally and return the normal template that should be
				 * Page.
				 */
				locate_block_template( 'full-width', 'page', $templates );

			}
		}

	},
	5
);

function kubio_change_customize_link_to_open_kubio_editor() {
	$kubio_url = add_query_arg( array( 'page' => 'kubio' ), admin_url( 'admin.php' ) );
	?>
	<script>
		(function () {
			var button = document.querySelector('.button.load-customize');

			if (button) {
				button.href = "<?php echo esc_url( $kubio_url ); ?>";
			}
		})();
	</script>
	<?php
}

add_action( 'welcome_panel', 'kubio_change_customize_link_to_open_kubio_editor', 20 );



function kubio_get_svg_kses_allowed_elements( $allowed_html = array() ) {

	$svg_elements = array(
		'svg'     =>
		array(
			'xmlns',
			'viewbox',
			'id',
			'data-name',
			'width',
			'height',
			'version',
			'xmlns:xlink',
			'x',
			'y',
			'enable-background',
			'xml:space',
		),
		'path'    =>
		array(
			'd',
			'id',
			'class',
			'data-name',
		),
		'g'       =>
		array(
			'id',
			'stroke',
			'stroke-width',
			'fill',
			'fill-rule',
			'transform',
		),
		'title'   =>
		array(),
		'polygon' =>
		array(
			'id',
			'points',
		),
		'rect'    =>
		array(
			'x',
			'y',
			'width',
			'height',
			'transform',
		),
		'circle'  =>
		array(
			'cx',
			'cy',
			'r',
		),
		'ellipse' =>
		array(
			'cx',
			'cy',
			'rx',
			'ry',
		),
	);

	$shared_attrs = array( 'data-*', 'id', 'class' );

	foreach ( $svg_elements as $element => $attrs ) {
		if ( ! isset( $allowed_html[ $element ] ) ) {
			$allowed_html[ $element ] = array();
		}

		$allowed_html[ $element ] = array_merge( $allowed_html[ $element ], array_fill_keys( array_merge( $attrs, $shared_attrs ), true ) );
	}

	return $allowed_html;

}

add_filter( 'wp_kses_allowed_html', 'kubio_get_svg_kses_allowed_elements' );


function kubio_plugin_meta( $plugin_meta, $plugin_file ) {
	$plugins_dir = trailingslashit( wp_normalize_path( WP_CONTENT_DIR . '/plugins/' ) );
	$kubio_file  = str_replace( $plugins_dir, '', wp_normalize_path( KUBIO_ENTRY_FILE ) );
	if ( $plugin_file === $kubio_file ) {
		$plugin_meta[0] = "{$plugin_meta[0]} (build: " . KUBIO_BUILD_NUMBER . ')';
	}

	return $plugin_meta;
}

add_filter( 'plugin_row_meta', 'kubio_plugin_meta', 10, 4 );
