<?php

use Kubio\Core\LodashBasic;
use Kubio\Core\Utils;

function kubio_replace_shared_style( $post_content, $styleRefs ) {
	$blocks = parse_blocks( $post_content );

	Utils::walkBlocks(
		$blocks,
		function ( &$block ) use ( $styleRefs ) {
			if ( ! str_contains( $block['blockName'], 'kubio' ) ) {
				return;
			}

			$template_parts = apply_filters( 'kubio/preview/template_part_blocks', array() );

			if ( in_array( $block['blockName'], $template_parts ) ) {
				return;
			}

			$main_attr       = LodashBasic::get( $block, 'attrs.kubio', array() );
			$block_style_ref = LodashBasic::get( $main_attr, 'styleRef' );
			if ( isset( $styleRefs[ $block_style_ref ] ) ) {
				if ( isset( $styleRefs[ $block_style_ref ]['style'] ) ) {
					LodashBasic::set( $block, 'attrs.kubio.style', $styleRefs[ $block_style_ref ]['style'] );
				}

				if ( isset( $styleRefs[ $block_style_ref ]['props'] ) ) {
					LodashBasic::set( $block, 'attrs.kubio.props', $styleRefs[ $block_style_ref ]['props'] );
				}
			}
		}
	);

	return wp_slash( serialize_blocks( $blocks ) );

}
function kubio_post_is_header( $post ) {
	$isHeader = false;
	if ( isset( $post ) && $post->post_type === 'wp_template_part' ) {
		$terms = get_the_terms( $post, 'wp_template_part_area' );
		if ( $terms ) {
			foreach ( $terms as $term ) {
				if ( $term->taxonomy === 'wp_template_part_area' && $term->slug === 'header' ) {
					$isHeader = true;
				}
			}
		}
	}
		return $isHeader;
}

function kubio_replace_shared_style_in_posts( $posts, $styleRefs ) {
	remove_filter( 'wp_insert_post_data', 'kubio_on_post_update' );

	try {
		foreach ( $posts as $post ) {
			$post_data                 = array();
			$post_data['ID']           = $post->ID;
			$post_data['post_content'] = kubio_replace_shared_style( $post->post_content, $styleRefs );
			$result                    = wp_update_post( $post_data, true );
		}
	} catch ( Exception $e ) {
	}

	add_filter( 'wp_insert_post_data', 'kubio_on_post_update', 10, 3 );
}

function kubio_count_style_refs( $post_content ) {
	$blocks    = parse_blocks( $post_content );
	$styleRefs = array();

	Utils::walkBlocks(
		$blocks,
		function ( $block ) use ( &$styleRefs ) {
			if ( ! isset( $block['blockName'] ) || ! str_contains( $block['blockName'], 'kubio' ) ) {
				return;
			}

			$block_style_ref = LodashBasic::get( $block, 'attrs.kubio.styleRef', false );

			if ( $block_style_ref ) {
				if ( ! isset( $styleRefs[ $block_style_ref ] ) ) {
					$styleRefs[ $block_style_ref ] = 0;
				}

				if ( $block_style_ref ) {
					$styleRefs[ $block_style_ref ] = $styleRefs[ $block_style_ref ] + 1;
				}
			}
		}
	);

	return $styleRefs;
}


function kubio_recursive_unset_empty_arrays( $source ) {
	if ( is_array( $source ) ) {
		foreach ( $source as $key => $value ) {
			$result = kubio_recursive_unset_empty_arrays( $value );
			if ( is_array( $result ) && empty( $result ) ) {
				unset( $source[ $key ] );
			} else {
				$source[ $key ] = $result;
			}
		}
	}

	return $source;
}

function kubio_get_third_party_blocks() {
	$thirdPartyBlocks = array(
		'core/archives',
		'core/calendar',
		'core/categories',
		'core/latest-comments',
		'core/latest-posts',
		'core/page-list',
		'core/rss',
		'core/search',
		'core/social-links',
		'core/tag-cloud',
	);

	return apply_filters( 'kubio/get_third_party_blocks', $thirdPartyBlocks );

}

function kubio_normalize_kubio_props( $post_content ) {
	$blocks             = parse_blocks( $post_content );
	$third_party_blocks = kubio_get_third_party_blocks();
	Utils::walkBlocks(
		$blocks,
		function ( &$block ) use ( $third_party_blocks ) {
			if ( ! str_contains( $block['blockName'], 'kubio' ) && ! in_array( $block['blockName'], $third_party_blocks ) ) {
				return;
			}

			$template_parts = apply_filters( 'kubio/preview/template_part_blocks', array() );

			if ( in_array( $block['blockName'], $template_parts ) ) {
				return;
			}

			$attrs          = LodashBasic::get( $block, 'attrs', array() );
			$block['attrs'] = apply_filters(
				'kubio/modify_block_attrs_on_save',
				kubio_recursive_unset_empty_arrays( $attrs ),
				$block
			);
		}
	);

	return array(
		wp_slash( serialize_blocks( $blocks ) ),
		$blocks,
	);

}

function kubio_collect_style_refs( $blocks ) {
	$styleRefs = array();

	Utils::walkBlocks(
		$blocks,
		function ( $block ) use ( &$styleRefs ) {
			if ( ! isset( $block['blockName'] ) || ! str_contains( $block['blockName'], 'kubio' ) ) {
				return;
			}
			$main_attr       = LodashBasic::get( $block, 'attrs.kubio', array() );
			$block_style_ref = LodashBasic::get( $main_attr, 'styleRef' );

			if ( $block_style_ref ) {
				$styleRefs[ $block_style_ref ] = $main_attr;
			}
		}
	);

	return $styleRefs;
}

function kubio_get_posts_with_style_refs( $style_refs, $exclude = array() ) {
	$query        = "SELECT * FROM wp_posts WHERE post_type != 'revision' AND (%s)";
	$like_clauses = array();
	$result       = array();
	foreach ( array_keys( $style_refs ) as $style_ref ) {
		$like_clauses[] = 'post_content LIKE \'%styleRef":"' . sanitize_text_field( $style_ref ) . '"%\'';
	}

	foreach ( $exclude as $exclude_index => $to_exclude ) {
		if ( is_numeric( $to_exclude ) ) {
			array_splice( $exclude, $exclude_index, 1 );
		}
	}

	if ( count( $exclude ) ) {
		$query .= sprintf( 'AND id NOT IN (%s) ', implode( ',', $exclude ) );
	}
	if ( count( $like_clauses ) ) {
		global $wpdb;
		$query = sprintf( $query, implode( ' OR ', $like_clauses ) );
		// the query is sanitized the OR clauses are composed
		// phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
		$result = $wpdb->get_results( $query );
	}

	return $result;
}

function kubio_update_meta( $post_ID, $post, $update ) {
	$styleRefsCount = kubio_count_style_refs( $post->post_content );
	// delete previous meta tu ensure it's unique
	delete_post_meta( $post_ID, 'styleRefs' );
	update_post_meta( $post_ID, 'styleRefs', $styleRefsCount );
}

function kubio_on_post_update( $data, $postarr, $unsanitized_postarr ) {
	if ( is_array( $unsanitized_postarr ) && isset( $unsanitized_postarr['post_content'] ) ) {
		$is_revision              = $data['post_type'] === 'revision';
		list( $content, $blocks ) = kubio_normalize_kubio_props( wp_unslash( $unsanitized_postarr['post_content'] ) );
		//only update shared style for the header template part
		$isHeader = kubio_post_is_header( (object) $postarr );

		if ( ! $is_revision && $isHeader ) {
			$style_refs      = kubio_collect_style_refs( $blocks );
			$exclude         = isset( $postarr['ID'] ) ? array( $postarr['ID'] ) : array();
			$posts_to_update = kubio_get_posts_with_style_refs( $style_refs, $exclude );
			kubio_replace_shared_style_in_posts( $posts_to_update, $style_refs );
		}

		$data['post_content'] = $content;
	}

	return $data;
}

add_filter( 'wp_insert_post_data', 'kubio_on_post_update', 10, 3 );
add_action( 'wp_insert_post', 'kubio_update_meta', 10, 3 );
