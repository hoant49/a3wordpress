<?php
/**
 * Plugin Name: Kubio
 * Plugin URI: https://kubiobuilder.com
 * Description: Kubio is an innovative block-based WordPress website builder that enriches the block editor with new blocks and gives its users endless styling options.
 * Author: ExtendThemes
 * Author URI: https://extendthemes.com
 * Version: 1.0.1
 * License: GPL3+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: kubio
 * Domain Path: /languages
 * Requires PHP: 7.1.3
 * Requires at least: 5.8.1
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


define( 'KUBIO_ENTRY_FILE', __FILE__ );
define( 'KUBIO_ROOT_DIR', plugin_dir_path( __FILE__ ) );
define( 'KUBIO_BUILD_DIR', plugin_dir_path( __FILE__ ) . '/build' );
define( 'KUBIO_VERSION', '1.0.1' );
define( 'KUBIO_BUILD_NUMBER', '19' );
define( 'KUBIO_LOGO_URL', plugins_url( '/static/kubio-logo.svg', __FILE__ ) );
define( 'KUBIO_LOGO_PATH', plugin_dir_path( __FILE__ ) . '/static/kubio-logo.svg' );
define( 'KUBIO_LOGO_SVG', file_get_contents( KUBIO_LOGO_PATH ) );

function kubio_dir_path() {
	return plugin_dir_path( __FILE__ );
}

function kubio_url( $path ) {
	return plugins_url( $path, __FILE__ );
}

/**
 * @var \Composer\Autoload\ClassLoader $kubio_autoloader ;
 */
global $kubio_autoloader;
$kubio_autoloader = require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

function kubio_does_not_support_current_theme() {
	$theme = wp_get_theme( get_template() );
	?>
	<div class="notice notice-warning">
		<p>
		<?php
			printf(
				// translators: %1$s - kubio page builder ,  %2$s - theme name
				__( '%1$s does not currently support the %2$s theme', 'kubio' ),
				'<strong>Kubio Page Builder</strong>',
				sprintf(
					'<strong>%s</strong>',
					$theme->get( 'Name' )
				)
			);
		?>
		</p>
	</div>
	<?php
}

function kubio_plugin_activated() {
	$experimental_options = get_option( 'gutenberg-experiments', array() );
	if ( ! is_array( $experimental_options ) ) {
		$experimental_options = array( $experimental_options );
	}
	$experimental_options['gutenberg-navigation']         = 1;
	$experimental_options['gutenberg-widget-experiments'] = 1;

	update_option( 'gutenberg-experiments', $experimental_options );
	do_action( 'kubio/plugin_activated' );
}

function kubio_plugin_init() {
	$template            = get_template();
	$supported_templates = array( 'kubio', 'elevate', 'elevate-wp' );

	if ( ! in_array( $template, $supported_templates ) ) {
		if ( is_admin() ) {
			add_action( 'admin_notices', 'kubio_does_not_support_current_theme' );
		}
		return;
	}

	require_once plugin_dir_path( __FILE__ ) . 'lib/load.php';

	add_theme_support( 'block-templates' );
	add_filter( 'kubio_is_enabled', '__return_true' );

	register_activation_hook( __FILE__, 'kubio_plugin_activated' );
}

kubio_plugin_init();
