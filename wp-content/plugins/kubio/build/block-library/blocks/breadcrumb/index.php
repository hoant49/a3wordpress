<?php

namespace Kubio\Blocks;

use Kubio\Core\Blocks\BlockBase;
use Kubio\Core\Registry;

class BreadcrumbBlock extends BlockBase {

	const OUTER = 'outer';

	public function initInternalData() {
		$colibri_breadcrumb_index = intval( get_theme_mod( 'colibri_breadcrumb_element_index', 0 ) );
		set_theme_mod(
			'colibri_breadcrumb_element_index',
			$colibri_breadcrumb_index === PHP_INT_MAX ? 0 : $colibri_breadcrumb_index + 1
		);
		$separatorSymbol = $this->getProp( 'separatorSymbol', '/' );
		$prefix          = $this->getProp( 'prefix', __( 'You are here:', 'kubio' ) );
		$usePrefix       = $this->getProp( 'usePrefix', '' );
		$isIcon          = $this->getProp( 'homeIcon.isIcon', 0 );
		$homeIcon        = $this->getProp( 'homeIcon.iconName', 'font-awesome/home' );
		$homeLabel       = $this->getProp( 'homeIcon.label', __( 'Home', 'kubio' ) );
		$atts            = array(
			'id'              => 'kubio-breadcrumb-' . ( $colibri_breadcrumb_index ),
			'separatorSymbol' => $separatorSymbol,
			'prefix'          => $prefix,
			'home_as_icon'    => $isIcon,
			'home_icon'       => $homeIcon,
			'home_label'      => $homeLabel,
			'use_prefix'      => $usePrefix,
		);
		return $atts;
	}

	public function mapPropsToElements() {
		//$shortcode = $this->getAttribute('shortcode');

			$atts    = $this->initInternalData();
			$content = kubio_breadcrumb_element_shortcode( $atts );

		return array(
			self::OUTER => array( 'innerHTML' => $content ),
		);
	}
	public function getEmptyShortcode() {
		ob_start();
		?>
		<p class="h-shortcode-placeholder-preview">Shortcode is empty</p>
		<?php
		return ob_get_clean();

	}
}

Registry::registerBlock(
	__DIR__,
	BreadcrumbBlock::class
);

