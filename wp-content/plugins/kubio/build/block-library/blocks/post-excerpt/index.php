<?php

namespace Kubio\Blocks;
use Kubio\Core\Blocks\BlockBase;
use Kubio\Core\Registry;
use IlluminateAgnostic\Arr\Support\Arr;

class PostExcerptBlock extends BlockBase {
	const TEXT = 'text';

	/**
	 * List of Kubio blocks which should be allowed in excerpts.
	 */
	const ALLOWED_EXCERPT_BLOCKS = array( 'kubio/text', 'kubio/heading' );

	/**
	 * Retrieves the maximum number of words declared by the user.
	 *
	 * @return numeric
	 */
	public function getExcerptWordCount() {
		return $this->getAttribute( 'wordCount', 16 );
	}

	/**
	 * This method will be passed to the `excerpt_allowed_blocks` filter and will add our ALLOWED_EXCERPT_BLOCKS
	 *
	 * @param $allowed_blocks array
	 *
	 * @return array
	 */
	public function excerpt_allowed_blocks( $allowed_blocks ) {
		array_push( $allowed_blocks, ...self::ALLOWED_EXCERPT_BLOCKS );
		return $allowed_blocks;
	}

	public function mapPropsToElements() {
		add_filter( 'excerpt_length', array( $this, 'getExcerptWordCount' ), 16 );
		add_filter( 'excerpt_allowed_blocks', array( $this, 'excerpt_allowed_blocks' ) );
		add_filter( 'get_the_excerpt', array( $this, 'maybeModifyExcerpt' ), 100, 2 );

		$post_id   = Arr::get( $this->block_context, 'postId', 0 );
		$post_type = Arr::get( $this->block_context, 'postType', 0 );

		//workaround for http://mantis.extendstudio.net/view.php?id=39609
		if ( $post_type === 'page' ) {
			$content = __( 'No post excerpt found', 'kubio' );
		} else {
			$content = ( get_the_excerpt( $post_id ) );
		}

		remove_filter( 'get_the_excerpt', array( $this, 'maybeModifyExcerpt' ), 100 );

		remove_filter(
			'excerpt_length',
			array( $this, 'getExcerptWordCount' )
		);

		$tag_name = 'p';
		return array(
			self::TEXT => array(
				'innerHTML' => $content,
				'tag'       => $tag_name,
			),
		);
	}

	public function maybeModifyExcerpt( $excerpt, $post ) {
		if ( empty( trim( $excerpt ) ) ) {
			$text         = strip_tags( $post->post_content );
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
			$excerpt      = wp_trim_words( $text, $this->getExcerptWordCount(), $excerpt_more );
		}

		return $excerpt;
	}
}


Registry::registerBlock(
	__DIR__,
	PostExcerptBlock::class,
	array(
		'metadata'        => '../text/block.json',
		'metadata_mixins' => array( './block.json' ),
	)
);

